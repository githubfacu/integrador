package com.CarRental.CarRental.util;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Builder
@Data
public class RecordWithTimestamp {
    private Long id;
    private Instant timestamp;

}
