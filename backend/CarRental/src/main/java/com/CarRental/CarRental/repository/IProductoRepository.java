package com.CarRental.CarRental.repository;

import com.CarRental.CarRental.model.Producto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IProductoRepository extends JpaRepository<Producto, Long> {
    Optional<Producto> findByName(String name);
    Optional<Producto> findById(Long id);
    Page<Producto> findAll(Specification<Producto> specification, Pageable pageable);

    @Query(value = "select * from PRODUCTOS where category_id =:id", nativeQuery = true)
    List<Producto> findByCategory(@Param("id") Long id);

}
