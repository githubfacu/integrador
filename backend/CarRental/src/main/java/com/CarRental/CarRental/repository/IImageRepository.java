package com.CarRental.CarRental.repository;

import com.CarRental.CarRental.model.Imagen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IImageRepository extends JpaRepository<Imagen, Long> {
    Optional<Imagen> findById(Long id);
    List<Imagen> findByProductId(Long id);

}
