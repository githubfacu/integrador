package com.CarRental.CarRental.controller;


import com.CarRental.CarRental.dto.product.ProductDtoCreate;
import com.CarRental.CarRental.dto.product.request.ProductRequest;
import com.CarRental.CarRental.dto.product.response.ProductCategoryResponse;
import com.CarRental.CarRental.dto.product.response.ProductResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import com.CarRental.CarRental.service.interfaces.IProductoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static com.CarRental.CarRental.util.GeneralConstansSwagger.RESPONSE_ERROR_400_ALREADY_EXIST_PRODUCT;

@Tag(name = "Producto", description = "Endpoints para la gestión de Productos")
@RequestMapping(value = "/products")
@RestController
public class ProductoController {
    private final IProductoService productoService;

    public ProductoController(final IProductoService productoService) {
        this.productoService = productoService;
    }
    @Operation(summary = "create producto", description = "enpoint encargado de registrar un producto")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "creacion exitosa de producto", value = "{\n" +
                            "  \"success\": true,\n" +
                            "  \"message\": \"string\",\n" +
                            "  \"currentProduct\": {\n" +
                            "    \"id\": 0,\n" +
                            "    \"name\": \"string\"\n" +
                            "    \"category\": \"string\"\n" +
                            "    \"brand\": \"string\"\n" +
                            "    \"model\": \"string\"\n" +
                            "    \"description\": \"string\"\n" +
                            "    \"fuel\": \"string\"\n" +
                            "    \"numPassengers\": 0,\n" +
                            "    \"numBags\": 0,\n" +
                            "    \"numDoors\": 0,\n" +
                            "    \"isAutomatic\": \"boolean\"\n" +
                            "    \"isAutomatic\": []"+
                            "  }}")
            })),
        @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                @ExampleObject(name = "already exist product", value = RESPONSE_ERROR_400_ALREADY_EXIST_PRODUCT)
        }))
    })
    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping
    public ProductResponse createProduct(final @RequestBody ProductDtoCreate request) throws ApiException {
        return productoService.createProduct(request);
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @DeleteMapping(value="/{id}")
    public ProductResponse deleteProduct(final @PathVariable  long id) throws ApiException {
        return productoService.deleteProduct(id);
    }

    @Operation(summary = "Update producto", description = "enpoint encargado de actualizar un producto")
    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PutMapping(value="/update")
    public ProductResponse updateProduct(final @RequestBody ProductDtoCreate request) throws ApiException {
        return productoService.updateProduct(request);
    }

    @Operation(summary = "buscador", description = "Retorna una lista de productos basado en criterio de busqueda, ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Lista de productos filtrados ", value = "{\n\"products\": [\n" +
                            "    {\n" +
                            "      \"id\": 0,\n" +
                            "      \"name\": \"string\",\n" +
                            "      \"category\": \"string\",\n" +
                            "      \"brand\": \"string\",\n" +
                            "      \"model\": \"string\",\n" +
                            "      \"description\": \"string\",\n" +
                            "      \"fuel\": \"string\",\n" +
                            "      \"numPassengers\": 0,\n" +
                            "      \"numBags\": 0,\n" +
                            "      \"numDoors\": 0,\n" +
                            "      \"isAutomatic\": true,\n" +
                            "      \"images\": [\n" +
                            "        {\n" +
                            "          \"id\": 0,\n" +
                            "          \"encodeImage\": \"string\",\n" +
                            "          \"product_id\": 0\n" +
                            "        }\n" +
                            "      ]\n" +
                            "    }\n" +
                            "  ],\n" +
                            "  \"pageable\": {\n" +
                            "    \"pageNumber\": 0,\n" +
                            "    \"pageSize\": 0,\n" +
                            "    \"totalElements\": 0,\n" +
                            "    \"sortBy\": \"string\",\n" +
                            "    \"direction\": \"ASC\"\n" +
                            "  }}")
            }))
    })
    @PostMapping(value="/public/get")
    public ProductResponse getProducts(final @RequestBody @io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "Example request body puedes combiar los datos que estan en el productCriteria\" +\n" +
                    "            \"\\\"criteria\\\": {\\n\" +\n" +
                    "            \"    \\\"name\\\": \\\"string\\\",\\n\" +\n" +
                    "            \"    \\\"category\\\": \\\"string\\\",\\n\" +\n" +
                    "            \"    \\\"brand\\\": \\\"string\\\",\\n\" +\n" +
                    "            \"    \\\"model\\\": \\\"string\\\",\\n\" +\n" +
                    "            \"    \\\"description\\\": \\\"string\\\",\\n\" +\n" +
                    "            \"    \\\"fuel\\\": \\\"string\\\",\\n\" +\n" +
                    "            \"    \\\"numPassengers\\\": 0,\\n\" +\n" +
                    "            \"    \\\"numBags\\\": 0,\\n\" +\n" +
                    "            \"    \\\"numDoors\\\": 0,\\n\" +\n" +
                    "            \"    \\\"isAutomatic\\\": true},  \"pageableDto\": {\n" +
                    "    \"pageNumber\": 0,\n" +
                    "    \"pageSize\": 0,\n" +
                    "    \"totalElements\": 0,\n" +
                    "    \"sortBy\": \"string\",\n" +
                    "    \"direction\": \"ASC\"\n" +
                    "  }",
            content = @Content(
                    mediaType = "application/json",
                    examples = {
                            @ExampleObject(
                                    name = "find by name ",
                                    value = "{\"criteria\": {\"name\": \"string\",  \"pageableDto\": {\n" +
                                            "    \"pageNumber\": 1,\n" +
                                            "    \"pageSize\": 1,\n" +
                                            "    \"sortBy\": \"name\",\n" +
                                            "    \"direction\": \"ASC\"\n" +
                                            "  }}}"
                            ),
                            @ExampleObject(
                                    name = "find by name y category",
                                    value = "{\"criteria\": {\"name\": \"string\",  \"category\": \"string\", \"pageableDto\": {\n" +
                                            "    \"pageNumber\": 1,\n" +
                                            "    \"pageSize\": 1,\n" +
                                            "    \"sortBy\": \"name\",\n" +
                                            "    \"direction\": \"ASC\"\n" +
                                            "  }}}"
                            )
                    }
            )
    )ProductRequest request){
        return productoService.getProducts(request);
    }

    @Operation(summary = "get Random Product", description = "Retorna maximo 10 productos aleatorios")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Lista aleatoria de productos", value = "{\n\"products\": [\n" +
                            "    {\n" +
                            "      \"id\": 0,\n" +
                            "      \"name\": \"string\"\n" +
                            "    }\n" +
                            "  ],\n" +
                            " id:\"string\"}")
            }))
    })
    @GetMapping("/public")
    public ProductResponse getRandomProducts(@RequestHeader(value = "ID", required = false) String id){
        return productoService.getRandomProducts(id);
    }

    @Operation(summary = "get product by id", description = "Retorna el producto encontrado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Current product", value = "{\n\"currentProduct\": \n" +
                            "    {\n" +
                            "      \"id\": 0,\n" +
                            "      \"name\": \"string\"\n" +
                            "    }\n" +
                            "  }")
            }))
    })
    @GetMapping (value="/public/{id}")
    public ProductResponse getById(final @PathVariable  long id) throws ApiException {
        return productoService.getProductById(id);
    }

    @GetMapping (value="/public/category/{id}")
    public ProductCategoryResponse getByCategory(final @PathVariable  long id) throws ApiException {
        return productoService.getProductsByCategory(id);
    }


}
