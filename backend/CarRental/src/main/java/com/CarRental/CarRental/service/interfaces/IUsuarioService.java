package com.CarRental.CarRental.service.interfaces;

import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.dto.usuario.LoginUserRequest;
import com.CarRental.CarRental.dto.usuario.LoginUserResponse;
import com.CarRental.CarRental.dto.usuario.RegisterRequest;
import com.CarRental.CarRental.dto.usuario.RegisterResponse;
import com.CarRental.CarRental.exception.ApiException;
import org.springframework.http.ResponseEntity;

public interface IUsuarioService {
    LoginUserResponse login(LoginUserRequest loginUserRequest) throws ApiException;
    RegisterResponse registerUser(RegisterRequest request)throws ApiException;

    RegisterResponse resendEmail(RegisterRequest request) throws ApiException;

    Response logout(String jwt)throws ApiException;
}
