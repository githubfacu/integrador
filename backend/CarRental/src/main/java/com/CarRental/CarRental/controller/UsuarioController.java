package com.CarRental.CarRental.controller;

import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.dto.usuario.LoginUserRequest;
import com.CarRental.CarRental.dto.usuario.LoginUserResponse;
import com.CarRental.CarRental.dto.usuario.RegisterRequest;
import com.CarRental.CarRental.dto.usuario.RegisterResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import com.CarRental.CarRental.service.interfaces.IUsuarioService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static com.CarRental.CarRental.util.GeneralConstansSwagger.*;

@Slf4j
@Tag(name = "Usuario", description = "Endpoints para la gestión de usuarios")
@RestController
@RequestMapping("/user")
public class UsuarioController {
    final  IUsuarioService usuarioService;

    public UsuarioController(IUsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @Operation(summary = "User Login", description = "Autentica a un usuario y devuelve información de inicio de sesión")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Login exitoso",
                    content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = LoginUserResponse.class))),
            @ApiResponse(responseCode = "401", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "bad_credential", value = RESPONSE_ERROR_401_BAD_CREDENTIAL)
            }))
    })
    @PostMapping("/public/login")
    public ResponseEntity<LoginUserResponse> loginUser(
            @Parameter(description = "Credenciales de login del Usuario", required = true) @RequestBody LoginUserRequest loginUserRequest) throws ApiException {
        LoginUserResponse loginUserResponse = usuarioService.login(loginUserRequest);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("user_login", "true");
        return ResponseEntity.ok()
                .headers(httpHeaders)
                .body(loginUserResponse);
    }

    @Operation(summary = "Register User", description = "Metodo encargado para registriar un usuario")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Registro existoro exitoso",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = RegisterResponse.class))),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "request_null", value = RESPONSE_ERROR_400_REQUEST_NULL),
                    @ExampleObject(name = "email_invalid", value = RESPONSE_ERROR_400_EMAIL_INVALID),
                    @ExampleObject(name = "password_invalid", value = RESPONSE_ERROR_400_PASSWORD_INVALID),
                    @ExampleObject(name = "name_invalid", value = RESPONSE_ERROR_400_NAME_INVALID),
                    @ExampleObject(name = "already_user", value = RESPONSE_ERROR_400_ALREADY_USER)
            }))
    })
    @PostMapping("/public/register")
    public RegisterResponse register(@RequestBody final RegisterRequest request) throws ApiException {
       return  this.usuarioService.registerUser(request);
    }

    @Operation(summary = "Reenviar email", description = "Metodo encargado para Reenviar un email")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Envio exitoso de email",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = RegisterResponse.class))),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "request_null", value = RESPONSE_ERROR_400_REQUEST_NULL),
                    @ExampleObject(name = "email_invalid", value = RESPONSE_ERROR_400_EMAIL_INVALID),
                    @ExampleObject(name = "notFoundEmail", value = RESPONSE_ERROR_400_NOT_FOUND_USER)
            }))
    })
    @PostMapping("/public/resendEmail")
    public RegisterResponse resendEmail(@RequestBody final RegisterRequest request) throws ApiException {
        return  this.usuarioService.resendEmail(request);
    }

    @PostMapping("/logout")
    @PreAuthorize("hasRole('ADMINISTRADOR') || hasRole('CLIENTE')")
    public Response login(@RequestHeader(name = HttpHeaders.AUTHORIZATION) String jwt) throws ApiException {

        return this.usuarioService.logout(jwt);
    }
}
