package com.CarRental.CarRental.service;

import com.CarRental.CarRental.dto.categoria.CategoriaRequest;
import com.CarRental.CarRental.dto.categoria.CategoriaResponse;
import com.CarRental.CarRental.dto.product.ProductDto;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.mapper.CategoryMapper;
import com.CarRental.CarRental.model.Categoria;
import com.CarRental.CarRental.model.Producto;
import com.CarRental.CarRental.repository.ICategoriaRepository;
import com.CarRental.CarRental.service.interfaces.ICategoriaService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.catalina.mapper.Mapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.CarRental.CarRental.exception.factory.CategoryFactoryException.*;

@Service
public class CategoriaService implements ICategoriaService {

    private final ICategoriaRepository categoriaRepository;
    private final ObjectMapper mapper;


    public CategoriaService(ICategoriaRepository categoriaRepository, ObjectMapper mapper) {
        this.categoriaRepository = categoriaRepository;
        this.mapper = mapper;
    }


    @Override
    public CategoriaResponse createCategoty(CategoriaRequest request) throws ApiException {

        //Validación por nombre, si existe se genera una excepcion
        Optional<Categoria> categoriaDto = categoriaRepository.findByName(request.getName());
        if(categoriaDto.isPresent()) throw ALREADY_EXIST_CATEGORY.toException();

        Categoria categorySaved = categoriaRepository.save(CategoryMapper.toEntity(request));
        request.setId(categorySaved.getId());

        return CategoriaResponse.builder()
                .success(true)
                .message("Categoría creada exitosamente")
                .build();
    }

    @Override
    public List<CategoriaRequest> getCategories() {
        List<CategoriaRequest> categories = new ArrayList<>();
        List<Categoria> categoriesBBDD = categoriaRepository.findAll();

        for (Categoria categoria: categoriesBBDD){
            categories.add(CategoryMapper.toDto(categoria));
        }
        /*
        for (int i = 0; i < categories.size(); i++) {
            categories.get(i).setProducts(mapper.convertValue(categoriesBBDD.get(i).getProductos(), new TypeReference<List<ProductDto>>() {
            }));
        }*/

        categories.forEach(category -> category.setProducts(mapper.convertValue(categoriesBBDD.
                get(categories.indexOf(category)).getProductos(), new TypeReference<List<ProductDto>>() {})));



        return categories;
       //return categoriaRepository.findAll().stream().map(CategoryMapper::toDto).toList();
    }

    @Override
    public CategoriaResponse deleteCategory(Long id) throws ApiException {
        //Validando si existe
        Categoria categoria =categoriaRepository.findById(id)
                .orElseThrow(DELETE_CATEGORY_ERROR_NOT_FOUND::toException);

        //Validando si tiene productos asociados
        Optional<Categoria> optionalCat = categoriaRepository.findById(id);
        if(!optionalCat.get().getProductos().isEmpty()) throw CATEGORY_IN_USE.toException();

        categoriaRepository.deleteById(id);

        return CategoriaResponse.builder()
                .success(true)
                .message("Categoría eliminada exitosamente")
                .build();
    }

    @Override
    public Categoria findById(Long id) throws ApiException {
        return categoriaRepository.findById(id)
                .orElseThrow(CATEGORY_NOT_FOUND::toException);
    }
}
