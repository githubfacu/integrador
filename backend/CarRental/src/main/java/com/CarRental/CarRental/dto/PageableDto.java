package com.CarRental.CarRental.dto;

import com.CarRental.CarRental.model.Producto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;


@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PageableDto {

    private int pageNumber;
    private int pageSize;
    //Este solo se usa en el response-
    private long totalElements;
    private long totalPage;
    private String sortBy;
    private Sort.Direction direction;

    public Pageable toPageable() {
        return PageRequest.of(pageNumber, pageSize, Sort.by(direction, sortBy));
    }

    public static PageableDto fromPageable(Page<Producto> productoPage) {
        Sort.Order order = productoPage.getSort().stream().findFirst().orElse(null);
        String sortBy = order != null ? order.getProperty() : null;
        Sort.Direction direction = order != null ? order.getDirection() : null;
        return PageableDto.builder()
                .pageNumber(productoPage.getNumber())
                .pageSize(productoPage.getSize())
                .totalPage(productoPage.getTotalPages())
                .direction(direction)
                .sortBy(sortBy)
                .totalElements(productoPage.getTotalElements())
                .build();
    }
}
