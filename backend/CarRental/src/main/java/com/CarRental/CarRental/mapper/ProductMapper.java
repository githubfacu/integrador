package com.CarRental.CarRental.mapper;

import com.CarRental.CarRental.dto.image.ImageRequest;
import com.CarRental.CarRental.dto.product.ProductDto;
import com.CarRental.CarRental.dto.product.ProductDtoCreate;
import com.CarRental.CarRental.dto.product.ProductoDtoCategory;
import com.CarRental.CarRental.model.Imagen;
import com.CarRental.CarRental.model.Producto;
import lombok.experimental.UtilityClass;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@UtilityClass
public class ProductMapper {

    public static Producto toEntity(ProductDto dto) {

        Producto producto = new Producto();
        producto.setId(dto.getId());
        producto.setName(dto.getName());
        producto.setCategory(CategoryMapper.toEntity(dto.getCategory()));
        producto.setBrand(dto.getBrand());
        producto.setModel(dto.getModel());
        producto.setDescription(dto.getDescription());
        producto.setFuel(dto.getFuel());
        producto.setNumPassengers(dto.getNumPassengers());
        producto.setNumBags(dto.getNumBags());
        producto.setNumDoors(dto.getNumDoors());
        producto.setIsAutomatic(dto.getIsAutomatic());
        producto.setImages(dto.getImages().stream().map(imageRequest -> toImagen(imageRequest, producto)).toList());

        return producto;
    }

    public static Producto toEntityCreate(ProductDtoCreate dto) {

        Producto producto = new Producto();
        producto.setId(dto.getId());
        producto.setName(dto.getName());
        producto.setBrand(dto.getBrand());
        producto.setModel(dto.getModel());
        producto.setDescription(dto.getDescription());
        producto.setFuel(dto.getFuel());
        producto.setNumPassengers(dto.getNumPassengers());
        producto.setNumBags(dto.getNumBags());
        producto.setNumDoors(dto.getNumDoors());
        producto.setIsAutomatic(dto.getIsAutomatic());
        producto.setImages(dto.getImages().stream().map(imageRequest -> toImagen(imageRequest, producto)).toList());

        return producto;
    }


    private static Imagen toImagen(final ImageRequest imageRequest, final Producto producto){
       final Imagen imagen = new Imagen();
       imagen.setEncodeImage(Base64.getUrlEncoder().encode(imageRequest.getEncodeImage().getBytes(StandardCharsets.UTF_8)));
       imagen.setId(imageRequest.getId());
       imagen.setProduct(producto);
        return imagen;
    }

    public static ProductDto toDto(Producto entity) {

        return ProductDto.builder().id(entity.getId())
                .name(entity.getName())
                .category(CategoryMapper.toDto(entity.getCategory()))
                .brand(entity.getBrand())
                .model(entity.getModel())
                .description(entity.getDescription())
                .fuel(entity.getFuel())
                .numPassengers(entity.getNumPassengers())
                .numBags(entity.getNumBags())
                .numDoors(entity.getNumDoors())
                .isAutomatic(entity.getIsAutomatic())
                .images(entity.getImages().stream().map(imagen->toDtoImagen(imagen)).toList())
                .build();
    }

    public static ProductoDtoCategory toDtoCategory(Producto entity) {

        return ProductoDtoCategory.builder().id(entity.getId())
                .name(entity.getName())
                .category_id(entity.getCategory().getId())
                .brand(entity.getBrand())
                .model(entity.getModel())
                .description(entity.getDescription())
                .fuel(entity.getFuel())
                .numPassengers(entity.getNumPassengers())
                .numBags(entity.getNumBags())
                .numDoors(entity.getNumDoors())
                .isAutomatic(entity.getIsAutomatic())
                .images(entity.getImages().stream().map(imagen->toDtoImagen(imagen)).toList())
                .build();
    }

    private static ImageRequest toDtoImagen(final Imagen imagen){

        return ImageRequest.builder()
                .id(imagen.getId())
                .encodeImage(new String( Base64.getUrlDecoder().decode(imagen.getEncodeImage()), StandardCharsets.UTF_8))
                .build();
    }
}
