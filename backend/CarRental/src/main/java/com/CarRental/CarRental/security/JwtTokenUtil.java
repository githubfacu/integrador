package com.CarRental.CarRental.security;

import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.Usuario;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;

import java.util.*;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import static com.CarRental.CarRental.exception.factory.UsuarioFactoryException.TOKEN_NOT_FOUND;
import static com.CarRental.CarRental.exception.factory.UsuarioFactoryException.tokenExpired;


@Component
public class JwtTokenUtil {
    @Value("${security.secret}")
    private String secret;
    @Value("${security.expiration}")
    private Long expiration;
    private Map<String, Usuario> listToken = new HashMap<>();

    public String generateToken(Usuario userDetails){
        final Date now = new Date();
        final Date expirationTime = new Date(now.getTime() +expiration);

        final String token = JWT.create()
                .withClaim("email", userDetails.getEmail())
                .withSubject(userDetails.getEmail())
                .withIssuedAt(now)
                .withExpiresAt(expirationTime)
                .sign(Algorithm.HMAC256(secret));
        listToken.put(token, userDetails);
        return token;
    }


    public void removeToken(String token){
        this.listToken.remove(token);
    }

    public UsernamePasswordAuthenticationToken validateToken(String token) throws ApiException {
        try{

            JWT.require(Algorithm.HMAC256(secret)).build().verify(token);

            Usuario userDetails = Optional.ofNullable(this.listToken.get(token))
                    .orElseThrow(TOKEN_NOT_FOUND::toException);



            return new UsernamePasswordAuthenticationToken(userDetails, token,
                    AuthorityUtils.createAuthorityList("ROLE_" + userDetails.getRole().name()));
        }
        catch (TokenExpiredException e){
            throw tokenExpired(e.getMessage()) .toException();
        }
    }

    public Optional<String> getTokenByEmail(String email) {
        return listToken
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue().getEmail().equals(email))
                .map(Map.Entry::getKey).findFirst();
    }
}