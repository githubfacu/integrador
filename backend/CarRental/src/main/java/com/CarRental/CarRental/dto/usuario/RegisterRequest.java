package com.CarRental.CarRental.dto.usuario;

import com.CarRental.CarRental.model.UsuarioRol;
import lombok.Data;

@Data
public class RegisterRequest {
    private String email;
    private String name;
    private String lastName;
    private String password;
    private UsuarioRol usuarioRol;
}
