package com.CarRental.CarRental.service;

import com.CarRental.CarRental.dto.PageableDto;
import com.CarRental.CarRental.dto.product.ProductDto;
import com.CarRental.CarRental.dto.product.ProductDtoCreate;
import com.CarRental.CarRental.dto.product.ProductoDtoCategory;
import com.CarRental.CarRental.dto.product.request.ProductCriteria;
import com.CarRental.CarRental.dto.product.request.ProductRequest;
import com.CarRental.CarRental.dto.product.response.ProductCategoryResponse;
import com.CarRental.CarRental.dto.product.response.ProductResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.mapper.ProductMapper;
import com.CarRental.CarRental.model.Categoria;
import com.CarRental.CarRental.model.Producto;
import com.CarRental.CarRental.repository.IProductoRepository;
import com.CarRental.CarRental.service.interfaces.IProductoService;
import com.CarRental.CarRental.util.RamdonProductUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.criteria.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.CarRental.CarRental.exception.factory.ProductFactoryException.*;
import static jakarta.servlet.http.HttpServletResponse.SC_NOT_FOUND;

@Service
@Slf4j
public class ProductoServiceImpl implements IProductoService {
    
    private final IProductoRepository productoRepository;
    private final ImageService imageService;

    private final CategoriaService categoriaService;

    private final ObjectMapper mapper;

    public ProductoServiceImpl(final IProductoRepository productoRepository, ImageService imageService, CategoriaService categoriaService, ObjectMapper mapper) {
        this.productoRepository = productoRepository;
        this.imageService = imageService;
        this.categoriaService = categoriaService;
        this.mapper = mapper;
    }
    /*
    public ProductResponse createProduct(final ProductRequest request) throws ApiException {
        final Optional<Producto> productDto = productoRepository
                .findByName(request.getCurrentProduct().getName());
        if(productDto.isPresent()) {
            throw ALREADY_EXIST_PRODUCT.toException();
        }


        final Producto save = productoRepository.save(ProductMapper.toEntity(request.getCurrentProduct()));
        request.getCurrentProduct().setId(save.getId());

        return ProductResponse.builder()
                .success(true)
                .message("Producto creado con exito")
                .currentProduct(request.getCurrentProduct())
                .build();
    }*/

    public ProductResponse createProduct(final ProductDtoCreate request) throws ApiException {
        final Optional<Producto> productDto = productoRepository
                .findByName(request.getName());
        if(productDto.isPresent()) {
            throw ALREADY_EXIST_PRODUCT.toException();
        }

        Producto producto = ProductMapper.toEntityCreate(request);
        producto.setCategory(categoriaService.findById(request.getCategory_id()));

        final Producto productoSaved = productoRepository.save(producto);

        return ProductResponse.builder()
                .success(true)
                .message("Producto creado con exito")
                .currentProduct(ProductMapper.toDto(productoSaved))
                .build();
    }

    public ProductResponse deleteProduct(final long id) throws ApiException {

        productoRepository
                .findById(id)
                .orElseThrow(DELETE_PRODUCT_ERROR_NOT_FOUND::toException);
        //TODO: si el producto esta asignado a una reserva toca deshabilitarlo y noo eliminarlo.
        // Hacer logica de buscar en las reservas activas o cerradas en mesnos de 1 mes, para ver si se elimina o no.
        productoRepository.deleteById(id);

        return ProductResponse.builder()
                .success(true)
                .message("Producto Eliminado con exito")
                .build();
    }

    public ProductResponse updateProduct(final ProductDtoCreate request) throws ApiException {

        productoRepository.findById(request.getId())
                .orElseThrow(()->new ApiException("Update_Error_not_found", "No se puede actualizar un producto que no existe", SC_NOT_FOUND));

        /*
        Dado que el Save de la linea 122 sobreescribe la entidad completa, esto
        tienen sentido, porque siempre va a pisar las imagenes que hay con unas nuevas
        (se verificó que cambia el ID siempre)
        final List<Long> nuevaListaImagenesIds = request.getImages().stream()
                .map(ImageRequest::getId)
                .collect(Collectors.toList());
        currentProduct.getImages().removeIf(imagen -> !nuevaListaImagenesIds.contains(imagen.getId()));
        productoRepository.save(currentProduct);*/


        Producto producto = ProductMapper.toEntityCreate(request);
        producto.setCategory(categoriaService.findById(request.getCategory_id()));

        final Producto productoSaved = productoRepository.save(producto);

        return ProductResponse.builder()
                .success(true)
                .message("Producto actualizado con exito")
                .currentProduct(ProductMapper.toDto(productoSaved))
                .build();

    }

    public ProductResponse getProducts(final  ProductRequest request) {

        final Pageable pageable = request.getCriteria().getPageableDto().toPageable();
        final Page<Producto> pageProduct = productoRepository

               .findAll(getSpecification(request.getCriteria()), pageable);
        final List<ProductDto> all = pageProduct.getContent().stream()
                .map(producto -> ProductMapper.toDto(producto))
                .collect(Collectors.toList());

        return ProductResponse.builder()
                .success(true)
                .products(all)
                .pageable(PageableDto.fromPageable(pageProduct))
                .build();
    }

    private Specification<Producto> getSpecification(final ProductCriteria criteria) {
        return (Root<Producto> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();


            if (criteria.getName() != null) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + criteria.getName() + "%"));
            }

            if (criteria.getCategory() != null) {
                Join<Producto, Categoria> categoriaJoin = root.join("category", JoinType.INNER);
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(categoriaJoin.get("name")), "%" + criteria.getCategory().toLowerCase() + "%"));
            }
            if (criteria.getBrand() != null) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("brand")), "%" + criteria.getBrand() + "%"));
            }
            if (criteria.getModel() != null) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("model")), "%" + criteria.getModel() + "%"));
            }
            if (criteria.getDescription() != null) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), "%" + criteria.getDescription() + "%"));
            }
            if (criteria.getFuel() != null) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("fuel")), "%" + criteria.getFuel() + "%"));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    public ProductResponse getRandomProducts(final String username) {
        final String id = Optional.ofNullable(username).orElse(UUID.randomUUID().toString());
        final List<Producto> producto  = productoRepository.findAll();
        return ProductResponse.builder()
                .products(RamdonProductUtil.getRamdomProduct(producto, id))
                .id(id)
                .success(true)
                .build();
    }

    @Override
    public ProductResponse getProductById(long id) throws ApiException {
        return ProductResponse.builder()
                .currentProduct(productoRepository.findById(id).map(ProductMapper::toDto).orElseThrow(PRODUCT_ERROR_NOT_FOUND::toException))
                .id( String.valueOf(id))
                .success(true)
                .build();
    }

    @Override
    public ProductCategoryResponse getProductsByCategory(Long category_id) throws ApiException {
        List<Producto> productsByCategory = productoRepository.findByCategory(category_id);
        if(productsByCategory.isEmpty()) throw PRODUCT_FOR_CATEGORY_NOT_FOUND.toException();

        List<ProductoDtoCategory> productDtos= productsByCategory.stream()
                .map(producto -> ProductMapper.toDtoCategory(producto))
                .collect(Collectors.toList());

        return ProductCategoryResponse.builder()
                .success(true)
                .products(productDtos)
                .build();
    }


}
