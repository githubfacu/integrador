package com.CarRental.CarRental.service.interfaces;

import com.CarRental.CarRental.dto.categoria.CategoriaRequest;
import com.CarRental.CarRental.dto.categoria.CategoriaResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.Categoria;

import java.util.List;
import java.util.Optional;

public interface ICategoriaService {
    CategoriaResponse createCategoty(CategoriaRequest request) throws ApiException;
    List<CategoriaRequest> getCategories();
    CategoriaResponse deleteCategory(Long id) throws ApiException;
    Categoria findById(Long id) throws ApiException;

}
