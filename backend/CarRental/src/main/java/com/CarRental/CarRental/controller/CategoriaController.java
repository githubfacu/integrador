package com.CarRental.CarRental.controller;

import com.CarRental.CarRental.dto.categoria.CategoriaRequest;
import com.CarRental.CarRental.dto.categoria.CategoriaResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.service.CategoriaService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.apache.log4j.Logger;

import java.util.List;

@RestController
@Tag(name = "Categoria", description = "Endpoints para la gestión de Categorias")
@RequestMapping(value = "/category")

public class CategoriaController {
    private final CategoriaService categoriaService;
    private final Logger LOGGER = Logger.getLogger(CategoriaController.class);

    public CategoriaController(CategoriaService categoriaService) {
        this.categoriaService = categoriaService;
    }

    @GetMapping("/public")
    public List<CategoriaRequest> getCategories(){
        return categoriaService.getCategories();
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMINISTRADOR')")
    public CategoriaResponse createCategory(@RequestBody CategoriaRequest request) throws ApiException {
        return categoriaService.createCategoty(request);
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ADMINISTRADOR')")
    public CategoriaResponse deleteCategory(@PathVariable Long id) throws ApiException{
        return categoriaService.deleteCategory(id);
    }


}
