package com.CarRental.CarRental.dto.usuario;


import com.CarRental.CarRental.model.Usuario;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class RegisterResponse {
    private boolean success;
    private String message;
    private String warning;
}
