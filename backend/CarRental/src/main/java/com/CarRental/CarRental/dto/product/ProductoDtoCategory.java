package com.CarRental.CarRental.dto.product;

import com.CarRental.CarRental.dto.image.ImageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductoDtoCategory {
    private Long id;
    private String name;
    private Long category_id;
    private String brand;
    private String model;
    private String description;
    private String fuel;
    private int numPassengers;
    private int numBags;
    private int numDoors;
    private Boolean isAutomatic;
    private List<ImageRequest> images;

}
