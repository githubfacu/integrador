package com.CarRental.CarRental.service.interfaces;

import com.CarRental.CarRental.dto.image.ImageRequest;
import com.CarRental.CarRental.dto.image.ImageResponse;
import com.CarRental.CarRental.dto.product.request.ProductRequest;
import com.CarRental.CarRental.exception.ApiException;

import java.util.List;

public interface IImageService {
    ImageResponse createImage(ImageRequest request, ProductRequest productRequest) throws ApiException;
    Boolean existById(Long id);
    ImageResponse deleteImage(Long id) throws ApiException;
    List<ImageRequest> getImages();



}
