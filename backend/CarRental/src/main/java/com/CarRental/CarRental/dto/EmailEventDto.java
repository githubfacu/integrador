package com.CarRental.CarRental.dto;

import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

@ToString
@Getter
public class EmailEventDto  extends ApplicationEvent {
    private String to;
    private String subject;
    private String body;

    public EmailEventDto(final Object source, final String to, final String subject, final String body) {
        super(source);
        this.body = body;
        this.subject = subject;
        this.to = to;
    }
}
