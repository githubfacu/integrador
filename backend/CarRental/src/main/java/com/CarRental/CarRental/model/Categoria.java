package com.CarRental.CarRental.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "categorias") // Nombre en la BBDD
public class Categoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Identificador único", example = "1L")
    private Long id;

    @Schema(description = "Nombre de la categoría", example = "Deportivo")
    private String name;

    @Schema(description = "Descripción asociada a la categoría.", example = "Este tipo de vehículos están diseñados para ofrecer velocidad y autonomía")
    private String description;

    //Mientras se mejora el tema de como manejar imagenes, se va a testear con URLs online
    @Lob
    @Schema(description = "Imagen asociada a la categoría")
    private byte[] imageEncode;
    //private String imageEncode;

    @OneToMany (mappedBy = "category", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    @Schema(description = "Lista de productos asociados a la categoria.")
    private List<Producto> productos = new ArrayList<>();

    public Categoria(Long id, String name, String description, byte[] imageEncode) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageEncode = imageEncode;
    }

    public void getImageEncode(byte[] encode) {
    }
}
