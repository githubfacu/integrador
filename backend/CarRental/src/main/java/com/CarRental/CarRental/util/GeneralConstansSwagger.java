package com.CarRental.CarRental.util;

import lombok.experimental.UtilityClass;


@UtilityClass
public class GeneralConstansSwagger {
//Productos
    public static final String RESPONSE_ERROR_400_ALREADY_EXIST_PRODUCT= "{\n" +
            "    \"error\": \"already_exist\",\n" +
            "    \"message\": \"Producto ya se encuentra regitrado en la base de datos\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";



//USUARIOS
    public static final String RESPONSE_ERROR_401_BAD_CREDENTIAL = "{\n" +
            "    \"error\": \"bad_credential\",\n" +
            "    \"message\": \"Credenciales invalidas: Usuario y/o Password invalid\",\n" +
            "    \"status\": 401,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_REQUEST_NULL = "{\n" +
            "    \"error\": \"request_null\",\n" +
            "    \"message\": \"El request no puede ser nulo\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_EMAIL_INVALID= "{\n" +
            "    \"error\": \"email_invalid\",\n" +
            "    \"message\": \"El email no puede ser nulo o no es valido\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_PASSWORD_INVALID= "{\n" +
            "    \"error\": \"password_invalid\",\n" +
            "    \"message\": \"El password es invalido. \nMínimo 8 caracteres de longitud, \nal menos una letra mayúscula, \nuna letra minúscula, \nun número y un carácter especial entre @#$%^&+=!\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_NAME_INVALID= "{\n" +
            "    \"error\": \"name_invalid\",\n" +
            "    \"message\": \"El nombre no puede ser nulo, o vacio\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_ALREADY_USER= "{\n" +
            "    \"error\": \"already_user\",\n" +
            "    \"message\": \"El email ****@***.com ya se encuentra registrado a otro usuario\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_NOT_FOUND_USER= "{\n" +
            "    \"error\": \"notFoundEmail\",\n" +
            "    \"message\": \"El email ****@***.com no se encuentra registrado\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";
}
