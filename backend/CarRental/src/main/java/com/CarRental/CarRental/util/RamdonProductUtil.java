package com.CarRental.CarRental.util;

import com.CarRental.CarRental.dto.product.ProductDto;
import com.CarRental.CarRental.mapper.ProductMapper;
import com.CarRental.CarRental.model.Producto;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class RamdonProductUtil {
    private static Map<String,Map<Long,Instant >> recordsByUser = new HashMap<String,Map<Long,Instant >>();
    private Set<RecordWithTimestamp> selectedRecords;
    private static Instant lastCleanupTime= Instant.now();
    private static final int MAX_RECORD = 10;
    private RamdonProductUtil(){
        this.lastCleanupTime = Instant.now();
    }

    public static final List<ProductDto>getRamdomProduct(final List<Producto> productos, final String username) {
       return getRamdomProduct1(productos, username)
                .stream()
                .map(producto->ProductMapper.toDto(producto))
                .collect(Collectors.toList());

    }

    private static  List<Producto>  getRamdomProduct1(final List<Producto> productos, final String username) {
        // Si hay menos de 10 productos, se baraja la lista y se retorna.
        if(productos.size()<=MAX_RECORD) {
             Collections.shuffle(productos);
            return productos;
        }

        // Si hay mas de 10 productos, se verifica si han pasado mas de 2 horas para ejecutar el limpiadoor y se actualiza la hora.
        final Map<Long,Instant > selectedRecords = recordsByUser.getOrDefault(username, new HashMap<>());
        //Verfiicar si tiene mas de 2 horas la ultima consulta para limpiar los seleccionados
        if(Duration.between(lastCleanupTime, Instant.now()).toHours()>=2) {
            cleanUpRecords(selectedRecords);
            lastCleanupTime = Instant.now();
        }
        // si hay registros previamente seleccionados y que no tengan mas 2 horas, se eliminan de la lista ded productos actual a retornar
        if(!selectedRecords.isEmpty() &&(productos.size()-selectedRecords.size())>=MAX_RECORD){
            productos.removeIf(record-> selectedRecords.containsKey(record.getId()));
        }
        if((productos.size()-selectedRecords.size())<MAX_RECORD){
            selectedRecords.clear();
        }
        //Si luego de eliminar los registros seleccionados, quedan menos de 10 registros. se borra la seleccioon y se
        // agregan los nuevos registros a la seleccion.
        if(productos.size()<=MAX_RECORD) {
            selectedRecords.clear();
            productos
                    .stream()
                    .parallel()
                    .forEach(producto -> selectedRecords.put(producto.getId(), Instant.now()));

            recordsByUser.put(username, selectedRecords);
            Collections.shuffle(productos);
            return productos;
        }

        List<Producto> randomRedords = new ArrayList<>();

        while (randomRedords.size() < MAX_RECORD) {
            int randomIndex = (int) (Math.random() * productos.size());
            Producto producto = productos.remove(randomIndex);
            randomRedords.add(producto);
            selectedRecords.put(producto.getId(), Instant.now());
        }
        recordsByUser.put(username, selectedRecords);
    return randomRedords;
    }

    // se recorren los registros anteriormente seleccionados aleatoreamente y se elimina de la seleccion
    // aquellos que tienen mas de 2 horas registrados.
    //duda probar
    private static void cleanUpRecords(Map<Long,Instant > selectedRecords ){
        if(selectedRecords.isEmpty()){
            return;
        }
        selectedRecords.entrySet().stream().forEach(x->{
            if (Duration.between(x.getValue(), Instant.now()).toHours() >= 2) {
                selectedRecords.remove(x.getKey());
            }
        });
    }
}
