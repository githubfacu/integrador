package com.CarRental.CarRental.service.interfaces;

import com.CarRental.CarRental.dto.product.ProductDtoCreate;
import com.CarRental.CarRental.dto.product.request.ProductRequest;
import com.CarRental.CarRental.dto.product.response.ProductCategoryResponse;
import com.CarRental.CarRental.dto.product.response.ProductResponse;
import com.CarRental.CarRental.exception.ApiException;

public interface IProductoService {
    //ProductResponse createProduct(final ProductRequest request) throws ApiException;
    ProductResponse createProduct(final ProductDtoCreate request) throws ApiException;
    ProductResponse deleteProduct(long id) throws ApiException;
    //ProductResponse updateProduct(final ProductRequest request) throws ApiException;
    ProductResponse updateProduct(final ProductDtoCreate request) throws ApiException;
    ProductResponse getProducts(final  ProductRequest request);
    ProductResponse getRandomProducts(String username);
    ProductResponse getProductById(long id) throws ApiException;
    ProductCategoryResponse getProductsByCategory (Long category_id) throws ApiException;
}
