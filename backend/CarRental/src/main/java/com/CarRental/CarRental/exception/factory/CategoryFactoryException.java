package com.CarRental.CarRental.exception.factory;

import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import lombok.experimental.UtilityClass;

import static jakarta.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static jakarta.servlet.http.HttpServletResponse.SC_NOT_FOUND;

@UtilityClass
public class CategoryFactoryException {

    public static final ApiErrorDto ALREADY_EXIST_CATEGORY =
            new ApiErrorDto("already_exist", "La categoría ya se encuentra registrada en la base de datos", SC_BAD_REQUEST);

    public static final ApiErrorDto DELETE_CATEGORY_ERROR_NOT_FOUND =
            new ApiErrorDto("delete_Error_not_found", "No se puede eliminar una categoría que no existe", SC_NOT_FOUND);

    public static final ApiErrorDto CATEGORY_IN_USE =
            new ApiErrorDto("category_in_use", "La categoría contiene productos asociados", SC_BAD_REQUEST);

    public static final ApiErrorDto CATEGORY_NOT_FOUND =
            new ApiErrorDto("category_not_found", "La categoría solicitada no se encuentra registrada", SC_BAD_REQUEST);
}
