package com.CarRental.CarRental.dto.product.request;

import com.CarRental.CarRental.dto.product.ProductDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductRequest {
    private ProductDto currentProduct;
    private ProductCriteria criteria;

}
