package com.CarRental.CarRental.dto.usuario;


public record LoginUserRequest(
        String email,
        String password
) { }
