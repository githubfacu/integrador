package com.CarRental.CarRental.mapper;

import com.CarRental.CarRental.dto.categoria.CategoriaRequest;
import com.CarRental.CarRental.dto.product.ProductDto;
import com.CarRental.CarRental.model.Categoria;
import com.CarRental.CarRental.model.Producto;
import lombok.experimental.UtilityClass;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@UtilityClass
public class CategoryMapper {

    public static Categoria toEntity(CategoriaRequest request){
        Categoria categoria = new Categoria();
        categoria.setId(request.getId());
        categoria.setName(request.getName());
        categoria.setDescription(request.getDescription());
        categoria.setImageEncode(Base64.getUrlEncoder().encode(request.getImageEncode().getBytes(StandardCharsets.UTF_8)));


        return categoria;
    }

    public static CategoriaRequest toDto(Categoria entity){
        return CategoriaRequest.builder()
                .id(entity.getId())
                .name(entity.getName())
                .description(entity.getDescription())
                .imageEncode(new String( Base64.getUrlDecoder().decode(entity.getImageEncode()), StandardCharsets.UTF_8))
                //.products(entity.getProductos().stream().map(ProductMapper::toDto).toList())
                .build();
    }


}
