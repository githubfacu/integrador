package com.CarRental.CarRental.exception.factory;

import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import lombok.experimental.UtilityClass;

import static jakarta.servlet.http.HttpServletResponse.*;


@UtilityClass
public class UsuarioFactoryException {

    public static final ApiErrorDto CREDENTIAL_INVALID =
            new ApiErrorDto("bad_credential", "Credenciales invalidas: Usuario y/o Password invalid", SC_UNAUTHORIZED);

    public static final ApiErrorDto REQUEST_NULL =
            new ApiErrorDto("request_null", "El request no puede ser nulo", SC_BAD_REQUEST);
    public static final ApiErrorDto EMAIL_INVALID =
            new ApiErrorDto("email_invalid", "El email no puede ser nulo o no es valido", SC_BAD_REQUEST);
    public static final ApiErrorDto PASSWORD_INVALID =
            new ApiErrorDto("password_invalid", "El password es invalido. \nMínimo 8 caracteres de longitud, y al menos \nun número", SC_BAD_REQUEST);

    public static final ApiErrorDto NAME_INVALID =
            new ApiErrorDto("name_invalid", "El nombre no puede ser nulo, o vacio", SC_BAD_REQUEST);

    public static final ApiErrorDto LASTNAME_INVALID =
            new ApiErrorDto("lastname_invalid", "El apellido no puede ser nulo, o vacio", SC_BAD_REQUEST);

    public static ApiErrorDto userAlreadyRegister(final String email) {
    return new ApiErrorDto("already_user", "El email "+email+" ya se encuentra registrado a otro usuario", SC_BAD_REQUEST);
    }

    public static ApiErrorDto notFoundEmail(final String email) {
        return new ApiErrorDto("notFoundEmail", "El email "+email+" no se encuentra registrado", SC_BAD_REQUEST);
    }

    public static ApiErrorDto tokenExpired(final String message) {
        return new ApiErrorDto("tokendExpired", message, SC_UNAUTHORIZED);
    }

    public static final ApiErrorDto TOKEN_NOT_FOUND =
            new ApiErrorDto("not_found_token", "El Token no se encuentra registrado", SC_UNAUTHORIZED);
    public static final ApiErrorDto INVALID_AUTHORIZATION_HEADER =
            new ApiErrorDto("invalidAuthorizationHeader", "Invalid authorization header", SC_BAD_REQUEST);

    public static ApiErrorDto forbiddend(final String message) {
        return new ApiErrorDto("forbidden", message, SC_FORBIDDEN);
    }
}
