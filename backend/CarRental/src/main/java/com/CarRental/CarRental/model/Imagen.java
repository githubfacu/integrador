package com.CarRental.CarRental.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import io.swagger.v3.oas.annotations.media.Schema;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Imagenes")
public class Imagen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Identificador unico de la imagen.", example = "10")
    private Long id;

    @Schema(description = "Blob imagen.", example = "iVBORw0KGgoAAAANSUhEUgAAAAUA\n" +
            "    AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO\n" +
            "    9TXL0Y4OHwAAAABJRU5ErkJggg==")
    @Lob
    private byte[] encodeImage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "id", nullable = false)
    @Schema(description = "Producto al que esta relacionada la imagen.")
    private Producto product;


}
