package com.CarRental.CarRental.exception.factory;

import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import lombok.experimental.UtilityClass;

import static jakarta.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static jakarta.servlet.http.HttpServletResponse.SC_NOT_FOUND;

@UtilityClass
public class ImageFactoryException {
    public static final ApiErrorDto ALREADY_EXIST_IMAGE =
            new ApiErrorDto("already_exist", "La Imagen ya se encuentra registrada en la base de datos", SC_BAD_REQUEST);
    public static final ApiErrorDto DELETE_IMAGE_ERROR_NOT_FOUND =
            new ApiErrorDto("delete_Error_not_found", "No se puede eliminar una imagen que no existe", SC_NOT_FOUND);

}
