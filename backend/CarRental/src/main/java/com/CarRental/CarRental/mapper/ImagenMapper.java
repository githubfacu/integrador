package com.CarRental.CarRental.mapper;

import com.CarRental.CarRental.dto.image.ImageRequest;
import com.CarRental.CarRental.model.Imagen;
import com.CarRental.CarRental.model.Producto;
import lombok.experimental.UtilityClass;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@UtilityClass
public class ImagenMapper {

    public static Imagen toEntity(ImageRequest dto, Producto producto) {

        Imagen imagen = new Imagen();
        imagen.setId(dto.getId());
        imagen.setEncodeImage( Base64.getUrlEncoder().encode(dto.getEncodeImage().getBytes(StandardCharsets.UTF_8)));
        imagen.setProduct(producto);

        return imagen;
    }

}
