package com.CarRental.CarRental.dto.usuario;

import com.CarRental.CarRental.model.UsuarioRol;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LoginUserResponse {
    private boolean success;
    private String userName;
    private String lastName;
    private String email;
    private UsuarioRol rol;
    private String token;
    private String message;
}
