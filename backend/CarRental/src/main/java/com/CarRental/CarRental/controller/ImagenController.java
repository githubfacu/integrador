package com.CarRental.CarRental.controller;


import com.CarRental.CarRental.dto.image.ImageRequest;
import com.CarRental.CarRental.dto.image.ImageResponse;
import com.CarRental.CarRental.dto.product.request.ProductRequest;
import com.CarRental.CarRental.dto.product.response.ProductResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import com.CarRental.CarRental.service.ImageService;
import com.CarRental.CarRental.service.interfaces.IImageService;
import com.CarRental.CarRental.service.interfaces.IProductoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.CarRental.CarRental.util.GeneralConstansSwagger.RESPONSE_ERROR_400_ALREADY_EXIST_PRODUCT;

@Tag(name = "Imagen", description = "Endpoints para la gestión de Imagenes")
@RequestMapping(value = "/images")
@RestController
public class ImagenController {
    private final ImageService imageService;

    public ImagenController(final ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping
    public ImageResponse createImage(@RequestBody ImageRequest request, ProductRequest productRequest) throws ApiException {
        return imageService.createImage(request, productRequest);
    }

    @GetMapping
    public List<ImageRequest> getImages(){
        return imageService.getImages();
    }

    @DeleteMapping(value="/{id}")
    public ImageResponse deleteImage(final @PathVariable  Long id) throws ApiException {
        return imageService.deleteImage(id);
    }



}
