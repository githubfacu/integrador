package com.CarRental.CarRental.exception.handler;

import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import jakarta.persistence.PersistenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.nio.file.AccessDeniedException;

import static com.CarRental.CarRental.exception.factory.UsuarioFactoryException.forbiddend;

/**
 * Este es un ControllerAdvice que sirve como inerceptor de excepciones de tipo ApiException lanzadas por los controlaldores
 * para transformar la respuesta que se da al cliente.
 */
@ControllerAdvice
public class ApiExceptionHandler {
  @ExceptionHandler(ApiException.class)
  protected ResponseEntity<Object> handleApiException(final ApiException apiException) {
    return new ResponseEntity<>(ApiErrorDto.fromException(apiException), HttpStatus.valueOf(apiException.getStatus()));
  }

  @ExceptionHandler(AccessDeniedException.class)
  public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex) {
    return new ResponseEntity<>(forbiddend(ex.getMessage()), HttpStatus.FORBIDDEN);
  }

  @ExceptionHandler(PersistenceException.class)
  public ResponseEntity<Object> handlePersistenceException(PersistenceException ex) {

    return new ResponseEntity<>(new ApiErrorDto("Jpa-error", ex.getMessage(), 500), HttpStatus.INTERNAL_SERVER_ERROR);
  }

}
