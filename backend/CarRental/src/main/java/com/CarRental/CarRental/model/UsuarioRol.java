package com.CarRental.CarRental.model;

public enum UsuarioRol {
    CLIENTE,
    AGENTE,
    ADMINISTRADOR
}
