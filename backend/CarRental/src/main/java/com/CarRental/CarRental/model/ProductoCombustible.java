package com.CarRental.CarRental.model;

public enum ProductoCombustible {
    GASOLINA,
    GASOIL,
    GNC,
    ELECTRICO
}
