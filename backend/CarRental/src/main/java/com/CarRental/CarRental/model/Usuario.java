package com.CarRental.CarRental.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "usr") // Especifica el nombre de la tabla en la base de datos
public class Usuario {

    @Id
    @Column(nullable = false)
    @Schema(description = "Dirección de correo electrónico del Usuario.", example = "john.doe@example.com")
    private String email;

    @Schema(description = "Nombre del Usuario.", example = "John")
    private String name;

    @Schema(description = "Apellido del Usuario.", example = "Doe")
    private String surname;



    @Column(nullable = false)
    @Schema(description = "Contraseña de la cuenta de usuario.", example = "password123", accessMode = Schema.AccessMode.WRITE_ONLY)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(nullable = true)
    @Schema(description = "Rol del usuario", example = "CLIENTE")
    private UsuarioRol role;
}
