package com.CarRental.CarRental.service;

import com.CarRental.CarRental.dto.EmailEventDto;
import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.dto.usuario.LoginUserRequest;
import com.CarRental.CarRental.dto.usuario.LoginUserResponse;
import com.CarRental.CarRental.dto.usuario.RegisterRequest;
import com.CarRental.CarRental.dto.usuario.RegisterResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.mapper.UsuarioMapper;
import com.CarRental.CarRental.model.Usuario;
import com.CarRental.CarRental.model.UsuarioRol;
import com.CarRental.CarRental.repository.IUsuarioRepository;
import com.CarRental.CarRental.security.JwtTokenUtil;
import com.CarRental.CarRental.service.interfaces.IUsuarioService;
import com.CarRental.CarRental.util.ValidatorUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.CarRental.CarRental.exception.factory.UsuarioFactoryException.*;
import static com.CarRental.CarRental.util.ValidatorUtil.validateEmail;

@Slf4j
@Service
@RequiredArgsConstructor

public class UsuarioService implements IUsuarioService {

    @Value("${url.login}")
    private String urlLogin;
    private static final Logger LOGGER = Logger.getLogger(UsuarioService.class);

    private final IUsuarioRepository usuarioRepository;

    private final ApplicationEventPublisher applicationEventPublisher;
    private final JwtTokenUtil jwtTokenUtil;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    @Override
    public LoginUserResponse login(LoginUserRequest loginUserRequest) throws ApiException {
        Usuario usuario = usuarioRepository.findByEmail(loginUserRequest.email())
                .orElseThrow(CREDENTIAL_INVALID::toException);

        if(!bCryptPasswordEncoder.matches(loginUserRequest.password(), usuario.getPassword())){
            throw CREDENTIAL_INVALID.toException();
        }

        LOGGER.info("Usuario [" + usuario.getName() + " " + usuario.getSurname() + "] Ha iniciado sesión correctamente.");

        return  LoginUserResponse.builder()
                .token(jwtTokenUtil.generateToken(usuario))
                .message("Login exitoso")
                .rol(usuario.getRole())
                .userName(usuario.getName())
                .lastName(usuario.getSurname())
                .email(usuario.getEmail())
                .success(true)
                .build();
    }

    @Override
    public RegisterResponse registerUser(final RegisterRequest request) throws ApiException {
        ValidatorUtil.validateRegister(request);
        final Optional<Usuario> usuario = usuarioRepository.findByEmail(request.getEmail());
        if(usuario.isPresent()){
            throw userAlreadyRegister(request.getEmail()).toException();
        }

        usuarioRepository.save(UsuarioMapper.toEntity(request));
        this.sendEmail(request.getEmail(), request.getName());
        return RegisterResponse.builder().success(true)
                .message("Su registro fue exitoso")
                .warning("A su email, llegara un correo electronico de confirmación, si no llega puede reintentar o envio un correo que no existe")
                .build();
    }

    @Override
    public RegisterResponse resendEmail(final RegisterRequest request) throws ApiException {
        validateEmail(request);
        Usuario user =usuarioRepository.findByEmail(request.getEmail())
                .orElseThrow(()->notFoundEmail(request.getEmail()).toException());
        this.sendEmail(request.getEmail(), user.getSurname());
        return RegisterResponse.builder().success(true)
                .message("Se reenvio exitosamente el email")
                .warning("A su email, llegara un correo electronico de confirmación, si no llega puede reintentar o envio un correo que no existe")
                .build();
    }

    @Override
    public Response logout(String jwt) throws ApiException {
        jwtTokenUtil.removeToken(jwt.substring(7));
        return Response.builder().success(true).message("Logout exitoso").build();
    }

    private void sendEmail(final String email, final String nombre){
        String body = "<html>" +
                "<head>" +
                "<title>Confirmacion registro exitoso</title>" +
                "</head>" +
                "<body>" +
                "<h1>Saludos! "+nombre+"</h1>" +
                "<p>Este es un mensaje de confirmacion que tu registro en Car Rental fue exitoso</p>" +
                "<p>Para poderte loguear en nuestra pagina web debes usar el siguiente email "+email+" con el password que decidiste al registrarte</p>" +
                "<p>El siguiente enlace te redirecciona al login:</p>" +
                "<p><a href=\"" + urlLogin + "\">Visitar el enlace</a></p>" +

                "</body>" +
                "</html>";

        EmailEventDto emailEventDto = new EmailEventDto(this, email, "Registro exitoso",body);
        applicationEventPublisher.publishEvent(emailEventDto);
    }

    public void changeRole(List<String> emails, UsuarioRol role) {
        final List<Usuario> users = this.usuarioRepository.findByEmailIn(emails)
                .stream()
                .map(usuario -> {
                    usuario.setRole(role);
                    return usuario;
                })
                .toList();
        this.usuarioRepository.saveAll(users);
        emails.forEach(email->{
            Optional<String> token= this.jwtTokenUtil.getTokenByEmail(email);
            token.ifPresent(this.jwtTokenUtil::removeToken);
        });
    }
}
