package com.CarRental.CarRental.controller;

import com.CarRental.CarRental.model.UsuarioRol;
import com.CarRental.CarRental.service.UsuarioService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


import java.util.Collections;
import java.util.List;

@Slf4j
@Tag(name = "Usuario", description = "Endpoints para la Administraci")
@RestController
@RequestMapping("/admin")
@PreAuthorize("hasRole('ADMINISTRADOR')")
public class AdminController {
    private final UsuarioService usuarioService;

    public AdminController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @PutMapping("/changeTo/role/{role}")
    public void changeRole(@RequestBody String email, @PathVariable UsuarioRol role){
            usuarioService.changeRole(Collections.singletonList(email), role);
    }

    @PutMapping("/changeTo/roles/{role}")
    public void changeRoles(@RequestBody List<String> emails, @PathVariable UsuarioRol role){
        usuarioService.changeRole(emails, role);
    }
}
