package com.CarRental.CarRental.service.interfaces;

public interface IEmailService {

    void sendEmail(String to, String subject, String body);
}
