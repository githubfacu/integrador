package com.CarRental.CarRental.dto.product.response;

import com.CarRental.CarRental.dto.PageableDto;
import com.CarRental.CarRental.dto.product.ProductDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.List;


@Data
@SuperBuilder
@ToString(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode(callSuper = true)
public class ProductResponse extends Response {
    private String id;
    private ProductDto currentProduct;
    private List<ProductDto> products;
    private PageableDto pageable;

}
