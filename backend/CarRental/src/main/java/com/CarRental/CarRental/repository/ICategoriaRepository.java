package com.CarRental.CarRental.repository;

import com.CarRental.CarRental.model.Categoria;
import com.CarRental.CarRental.model.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ICategoriaRepository extends JpaRepository<Categoria, Long> {
    Optional<Categoria> findByName(String name);
    Optional <Categoria> findById(Long id);
}
