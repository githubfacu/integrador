package com.CarRental.CarRental.dto.categoria;

import com.CarRental.CarRental.dto.product.ProductDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CategoriaRequest {
    private Long id;
    private String name;
    private String description;
    private String imageEncode;
    private List<ProductDto> products;
}
