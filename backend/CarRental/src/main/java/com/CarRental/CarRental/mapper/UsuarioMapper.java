package com.CarRental.CarRental.mapper;

import com.CarRental.CarRental.dto.usuario.RegisterRequest;
import com.CarRental.CarRental.model.Usuario;
import com.CarRental.CarRental.model.UsuarioRol;
import lombok.experimental.UtilityClass;

import java.util.Optional;

@UtilityClass
public class UsuarioMapper {

    public static Usuario toEntity(final RegisterRequest request){
        final Usuario usuario = new Usuario();
        usuario.setName(request.getName());
        usuario.setEmail(request.getEmail());
        usuario.setSurname(request.getLastName());
        usuario.setPassword(request.getPassword());
        usuario.setRole(Optional.ofNullable(request.getUsuarioRol()).orElse(UsuarioRol.CLIENTE));
        return usuario;
    }
}
