package com.CarRental.CarRental.util;

import com.CarRental.CarRental.dto.product.ProductDto;
import com.CarRental.CarRental.model.Imagen;
import com.CarRental.CarRental.model.Producto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RamdonProductUtilTest {
    List<Producto> productos = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        /*productos.add(new Producto(1L, "Producto 1",""));
        productos.add(new Producto(2L, "Producto 2",""));
        productos.add(new Producto(3L, "Producto 3",""));*/
    }

    private List<Imagen> crearImagenesParaProducto(Producto producto, int count) {
        List<Imagen> imagenes = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            Imagen imagen = new Imagen();
            //imagen.setEncodeImage("assets/Images/" + producto.getName().toLowerCase().replace(" ", "-") + "-img" + i + ".jpg");
            imagen.setProduct(producto);
            imagenes.add(imagen);
        }
        return imagenes;
    }


    @Test
    public void givenListOfProductsLessThanTenWhenCallGetRamdomProducThenReturnList(){

        List<ProductDto>  list =RamdonProductUtil.getRamdomProduct(productos,"melissa");
        assertTrue(list.size()<10);
        assertEquals(productos.size(), list.size());
    }

}
