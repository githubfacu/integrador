# Integrador

## Components

- **CI/CD**:
  - [Read more about CI/CD](_cicd/README.md)
- **Backend**:
  - [Read more about the backend](backend/README.md)
- **Frontend**:
  - [Read more about the frontend](frontend/README.md)
- **UX/UI**:
  - [Read more about UX/UI](uxui/README.md)
