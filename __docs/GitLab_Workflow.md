# Guía de Flujo de Trabajo con GitLab

Esta guía proporciona una visión general de los pasos recomendados para trabajar en proyectos usando GitLab, desde la actualización de tu rama principal, pasando por el desarrollo de características, hasta la creación y fusión de Solicitudes de Fusión (Merge Requests, MRs).

## 1. Actualizar desde el Remoto

Antes de comenzar a trabajar en nuevas características o correcciones, asegúrate de que tu rama principal esté actualizada:

```
git checkout main
git pull origin main
```

## 2. Crear una Nueva Rama

Para cada nueva característica o corrección, crea una rama separada:

```
git checkout -b tu-rama-de-trabajo
```

### 2.1 Nomenclatura de la Rama

Al nombrar tu rama de trabajo, sigue una nomenclatura específica para indicar el ámbito de los cambios:

Frontend: Utiliza el prefijo frontend- para cambios en el frontend. Ejemplo: 

```
git checkout -b frontend-nueva-caracteristica
```

Backend: Utiliza el prefijo backend- para cambios en el backend. Ejemplo:

```
git checkout -b backend-correccion-error
```

Esta nomenclatura organiza e identifica rápidamente el propósito y el área de impacto de cada rama, facilitando la gestión del proyecto y la colaboración entre equipos.



## 3. Realizar Cambios

Desarrolla tu característica o corrección en tu entorno local. Es recomendable hacer pulls frecuentes de la rama `main` para asegurarte de que tu rama de trabajo esté actualizada y reducir la posibilidad de conflictos durante la fusión:

```
git checkout tu-rama-de-trabajo
git pull origin main
```

## 4. Preparar y Confirmar Cambios

Una vez completados tus cambios, agrégalos y confírmalos:

```
git add .
git commit -m "Mensaje descriptivo de los cambios realizados"
```

## 5. Subir tu Rama al Remoto

Sube tu rama y sus cambios al repositorio remoto:

```
git push -u origin tu-rama-de-trabajo
```

## 6. Crear una Solicitud de Fusión (MR)

Para integrar tus cambios en la rama principal, sigue estos pasos detallados:

1. En la interfaz de GitLab, navega al proyecto correspondiente.
2. Dirígete a la sección "Merge Requests" o "Solicitudes de fusión" en el menú lateral.
3. Haz clic en "New Merge Request" o "Nueva solicitud de fusión".
4. Elige tu rama de trabajo en "Source branch" o "Rama fuente" y `main` como "Target branch" o "Rama objetivo".
5. Llena los campos del formulario de la MR con:
   - **Title**: Un título claro y conciso.
   - **Description**: Una descripción detallada de los cambios realizados.
   - **Assignees**: Asigna responsables de revisar la MR, si es necesario.
   - **Labels**: Etiqueta la MR para categorizar (opcional).
6. Envía la MR haciendo clic en "Submit merge request" o "Enviar solicitud de fusión".

## 7. Revisión y Fusión

1. Espera a que el equipo revise la MR, proporcionando comentarios o aprobación.
2. Si se requieren cambios, actualiza tu rama con las modificaciones y súbelas al remoto.
3. Tras la aprobación y el éxito de todos los tests, fusiona la MR a `main`. Considera marcar la opción para eliminar la rama fuente una vez aceptada la MR.
