# Guía de Etiquetado para Despliegues

El uso de etiquetas (tags) en GitLab es crucial para marcar versiones específicas para despliegues y mantener un registro claro del progreso del desarrollo. Esta guía detalla cómo utilizar las etiquetas siguiendo el esquema semántico de versionado y los prefijos de características.

## Semántica de Versiones

- **MAYOR**: Se incrementa cuando los cambios son incompatibles con versiones anteriores o son actualizaciones mayores.
- **MENOR**: Se incrementa con la adición de nuevas funcionalidades de manera compatible con versiones anteriores.
- **PARCHE**: Se incrementa con correcciones de errores que son compatibles con versiones anteriores.

## Convenciones de Etiquetas

Las etiquetas deben seguir el formato `componente-vX.Y.Z-IDENTIFICADOR`, donde:

- `componente`: puede ser `frontend` o `backend`, según la parte del proyecto a la que afecta el lanzamiento.
- `vX.Y.Z`: sigue el esquema de versión semántica, donde `X` es la versión mayor, `Y` es la versión menor, y `Z` es el número de parche.
- `IDENTIFICADOR`: es un sufijo opcional que indica el tipo de lanzamiento o la naturaleza de los cambios:
  - Para `frontend`:
    - `FEAT`: Nueva característica.
    - `HOTFIX`: Corrección urgente de errores.
    - `EXP`: Lanzamiento experimental.
    - `STABLE`: Todas las pruebas OK.
  - Para `backend`:
    - `API`: Cambio en la API.
    - `SEC`: Actualización de seguridad.
    - `DB`: Actualización relacionada con la base de datos.
    - `REF`: Refactorización de código existente.

## Ejemplos de Etiquetas

- **Nueva Característica en Frontend**:
  ```sh
  git tag -a frontend-v1.1.0-FEAT -m "Nueva característica en frontend"
  git push origin frontend-v1.1.0-FEAT
  ```
- **Corrección Urgente en Backend**:
  ```sh
  git tag -a backend-v1.0.1-HOTFIX -m "Corrección urgente en backend"
  git push origin backend-v1.0.1-HOTFIX
  ```
- **Lanzamiento Experimental en Frontend**:
  ```sh
  git tag -a frontend-v1.2.0-EXP -m "Lanzamiento experimental en frontend"
  git push origin frontend-v1.2.0-EXP
  ```

Sigue esta convención para asegurar que los despliegues y versiones en tu proyecto sean consistentes, trazables y claros para todo el equipo.