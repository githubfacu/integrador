# Car Rental Application Deployment Script

This script automates the deployment of the Car Rental application by pulling the latest Docker images for both the frontend and backend services from Docker Hub, generating a `docker-compose.yml` file, and running the services using Docker Compose.

## Prerequisites

- **Docker and Docker Compose** must be installed on the machine where the script is executed. Docker Compose is used to orchestrate the multi-container application.
- **jq** must be installed. It is used to parse JSON data from Docker Hub API responses.
- **Curl** must be installed to fetch tags from Docker Hub.
- The script must have **execution permissions** set. You can grant these permissions by running `chmod +x car-rental.sh`.

## Usage

To use this script, you can execute it with either the `up` or `down` command-line argument:

- `./car-rental.sh up`: This command fetches the latest Docker image tags for the frontend and backend from Docker Hub, prompts the user to select a tag for each, generates a `docker-compose.yml` file with the selected images, and starts the application using `docker-compose up -d`.
- `./car-rental.sh down`: This command stops the running services and removes the containers using `docker-compose down`.

### Selecting Docker Image Tags

When running the script with the `up` command, it will automatically fetch the latest tags for the frontend and backend images from Docker Hub. If there are multiple tags available, the script will list them and prompt the user to select one for each service. There is a 5-second timeout for each selection; if no input is provided within this period, the first tag in the list will be automatically selected.

## Additional Notes

- Ensure your Docker and Docker Compose installations are up to date to avoid any compatibility issues.
- The generated `docker-compose.yml` file will be saved in the current directory. If a file with the same name already exists, it will be overwritten.
- This script assumes that the Docker images for the frontend and backend are publicly available on Docker Hub. If the images are hosted in a private repository, you may need to log in to Docker Hub before running the script.

## Troubleshooting

- If you encounter permission issues when running the script, ensure that it has the correct execution permissions and that you have the necessary privileges to execute Docker commands on your machine.
- For problems related to Docker or Docker Compose, refer to their official documentation or troubleshooting guides.

