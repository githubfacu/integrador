import React, { useEffect, useState } from 'react'
import PasarPaginas from '../Componentes/PasarPaginas'
import {categoriaDeVehiculo, seccionVehiculos} from '../Estilos/Categorias.module.css'
import Carddetalle from '../Componentes/Carddetalle'


const Categorias = () => {

    const url = 'http://localhost:8080'
    const endpoint = 'http://3.83.149.57:8080'

    const [vehiculos, setVehiculos] = useState([])
    const [categoria, setCategoria] = useState('')


    useEffect(() => {
        const categoriaSession = JSON.parse(sessionStorage.getItem('categoria'))
        fetch(url)
        .then(res=>{
            if(res.ok){
                return res.json()
            }
            else{
                throw new Error(`Error en la solicitud: ${res.status}`)
            }
        })
        .then(data => {
            console.log(data)

            const vehiculosFiltrados = data.filter((vehiculo) => {
                if(vehiculo.categoria === 'Sedan' && categoriaSession === 'SEDAN'){
                    return true;
                }
                if(vehiculo.categoria === 'Pickup' && categoriaSession === '4X4'){
                    return true;
                }
                if(vehiculo.categoria === 'Compacto' && categoriaSession === 'COMPACTO'){
                    return true;
                }
                if(vehiculo.categoria === 'Deportivo' && categoriaSession === 'CONVERTIBLE'){
                    return true;
                }
                if(vehiculo.categoria === 'SUV' && categoriaSession === 'SUV'){
                    return true;
                }
                if(vehiculo.combustible === 'ELECTRICO' && categoriaSession === 'ELÉCTRICOS'){
                    return true;
                }
                return false;
            });
    
            setVehiculos(vehiculosFiltrados);
            setCategoria(categoriaSession)            
        })
        .catch(error=>console.error(error))


    }, [])





    const vehiculosPorPagina = 10
    const [paginaActual, setPaginaActual] = useState(1)
    const cantidadDePaginas = Math.ceil(vehiculos.length/vehiculosPorPagina)

    const inicio = (paginaActual - 1) * vehiculosPorPagina
    const fin = inicio + vehiculosPorPagina
    const vehiculosMostrados = vehiculos.slice(inicio, fin)

    const cambiarPagina = (nuevaPagina) => {
    setPaginaActual(nuevaPagina)}

    function avanzar(){
        cambiarPagina(paginaActual + 1)
    }

    function retroceder(){
        cambiarPagina(paginaActual - 1)
    }


  return (
    <div className={categoriaDeVehiculo}>
        <h1>{categoria}</h1>
        <section className={seccionVehiculos}>
            <ul>
                {/* {vehiculos.length > 0 &&
                    vehiculos.map((vehiculo)=>
                        <li key={vehiculo.id}>
                            <Carddetalle vehiculo={vehiculo}/>
                        </li>
                    )} */}
            </ul>

        </section>
        <div>
            <PasarPaginas
                paginaActual={paginaActual}
                setPaginaActual={setPaginaActual}
                cantidadDePaginas={cantidadDePaginas} 
                avanzar={avanzar} 
                retroceder={retroceder}
            />
        </div>
    </div>
  )
}

export default Categorias