import React, { useEffect , useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { navegacion, contenedor, oculto, formulario, registro, caracteristicas } from '../../Estilos/Administracion/AgregarCategoria.module.css'
import useInput from '../../Hooks/useInput'
import ElementoDeLista from '../../Componentes/ElementoDeLista'
import { toast } from 'react-toastify'


const AgregarCategoria = () => {

  const url = 'http://localhost:8080'
  const endpoint = 'http://3.83.149.57:8080'

  const entidad = 'Categoria'

  const [categoriasEnBaseDeDatos, setCategoriasEnBaseDeDatos] = useState([])
  const token = JSON.parse(localStorage.getItem('token'))

  console.log(token);

  const navigate = useNavigate()

  const irAtras = () => {
    navigate(-1)
  }

  const nombreCategoria = useInput('text')
  const descripcion = useInput('text')
  const [imagenBase64, setImagenBase64] = useState('');

  const imgOnChange = (event) => {
    const archivoSeleccionado = event.target.files[0];
    if (archivoSeleccionado) {
      const reader = new FileReader();
      reader.readAsDataURL(archivoSeleccionado);
      reader.onloadend = () => {
        setImagenBase64(reader.result);
      };
    }
  };

  const payload = {
    name: nombreCategoria.value,
    description: descripcion.value,
    imageEncode: imagenBase64
  }

  const configuraciones = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify(payload)
  }



  const enviarFormulario = (e) => {
    e.preventDefault()

    console.log(payload);
    console.log(configuraciones);

    fetch(`${url}/category`,configuraciones)
    .then(res=>{
      if(!res.ok){
        throw new Error (res.status)
      }
      else{
        return res.json()
      }
    })
    .then((data)=>{
      console.log(data)
      toast.success('Categoria registrada en Base de Datos con éxito!', {
        position: "bottom-left",
        autoClose: 7000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: 0,
        theme: "dark",
      });
    })
    .catch((error)=>{
      console.error('Error!', error)
      return toast.error('Error al enviar el formulario', {
        position: "bottom-left",
        autoClose: 7000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: 0,
        theme: "dark",
      });
    })
  }


  const configuracionesGet = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  }

  useEffect(() => {
    fetch(`${url}/category/public`, configuracionesGet)
    .then(res=>{
      if(!res.ok){
        throw new Error(res.status)
      }
      else{
        console.log(res);
        return res.json()
      }
    })
    .then((data)=>{
      console.log(data);
      setCategoriasEnBaseDeDatos(data)
    })
    .catch((error)=>console.error(error))

  }, []);




  return (
    <>
      <div className={contenedor}>
        <div className={navegacion}>
          <h3>Panel de Administracion</h3>
          <span onClick={irAtras} style={{cursor: 'pointer', }}><i style={{color: '#373866'}} className="fa-solid fa-arrow-left fa-2xl"></i></span>   
        </div>
        <h2>Registro de Categorías</h2>
        <div className={registro} >

          <form onSubmit={enviarFormulario}  className={formulario} >
            <label htmlFor="categoryName">Nombre de la Categoría:</label>
            <input {...nombreCategoria}/>

            <textarea {...descripcion} placeholder='Descripcion' ></textarea>

            <label htmlFor="agregarImagen">Agregar Imagen:</label>
            <input type="file" accept="image/jpeg,image/png" id="imgCarga" onChange={imgOnChange} />

            <button type="submit">Enviar</button>
          </form>

          <section>
            <div className={caracteristicas}>
              <h4 style={{width: '15%'}}>ID</h4>
              <h4 style={{width: '40%'}}>Categoria</h4>
              <h4 style={{width: '45%'}}>Acciones</h4>
            </div>
            <ul>
              {
                categoriasEnBaseDeDatos && 
                categoriasEnBaseDeDatos.map((item) => {
                  return <li key={item.id}>
                  <ElementoDeLista token={token} identificacion={item.id} nombre={item.name} url={`${url}/category`} entidad={entidad}/>
                    </li>
                })
              }
            </ul>
          </section>

        </div>
      </div>

      <div className={oculto}>
        <h2>Panel de Administracion no disponible en dispositivos móviles</h2>
      </div>
    </>
  )
}

export default AgregarCategoria