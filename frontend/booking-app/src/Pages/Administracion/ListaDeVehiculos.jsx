import React, { useState, useEffect } from 'react'
import { navegacion, contenedor, oculto, caracteristicas } from '../../Estilos/Administracion/ListaDeVehiculos.module.css'
import ElementoDeLista from '../../Componentes/ElementoDeLista'
import { useNavigate } from 'react-router-dom'


const ListaDeVehiculos = () => {

  const navigate = useNavigate()
  const [vehiculos, setVehiculos] = useState([])
  const url = 'http://localhost:8080'
  const endpoint = 'http://3.83.149.57:8080'

  const token = localStorage.getItem('token')

  const entidad = 'Vehiculo'

  const configuraciones = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  }

  useEffect(() => {
    fetch(`${url}/products/public`)
    .then(res=>{
      return res.json()
    })
    .then((data)=>setVehiculos(data.products))
  }, [])

  const irAtras = () => {
    navigate(-1)
  }


  console.log(vehiculos);

  return (
    <>
      <div className={contenedor}>

        <div className={navegacion}>
          <h3>Panel de Administracion</h3>
          <span onClick={irAtras} style={{cursor: 'pointer'}}><i style={{color: '#373866'}} className="fa-solid fa-arrow-left fa-2xl"></i></span>    
        </div>

        <h2>Vehiculos Registrados</h2>
        <article>
          <div className={caracteristicas}>
            <h4 style={{width: '15%'}}>ID</h4>
            <h4 style={{width: '40%'}}>Auto</h4>
            <h4 style={{width: '45%'}}>Acciones</h4>
          </div>

          <ul>
            {vehiculos && 
              vehiculos.map((vehiculo) => {
                return <li key={vehiculo.id}>
                  <ElementoDeLista token={token} identificacion={vehiculo.id} nombre={vehiculo.brand+vehiculo.model}  url={`${url}/products`} entidad={entidad}/>
                </li>
              })
            }
          </ul>

        </article>

      </div>


      <div className={oculto}>
        <h2>Panel de Administracion no disponible en dispositivos móviles</h2>
      </div>
    </>
  )
}

export default ListaDeVehiculos