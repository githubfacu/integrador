import React from 'react'
import { useNavigate } from 'react-router-dom'
import administracion from '../../Estilos/Administracion/Administracion.module.css'


const Administracion = () => {

  const navigate = useNavigate()

  const navegarAagregarVehiculo = ()=>{
    navigate("/administracion/agregarvehiculo")
  }

  const navegarAlistarVehiculos = ()=>{
    navigate("/administracion/gestiondevehiculos")
  }
  const navegarAgregarCategoria = ()=>{
    navigate("/administracion/agregarcategoria")
  }


  return (
    <>
      <div className={administracion.container}>
          <h1>ADMINISTRACION</h1>
          <div className={administracion.panel}>
            <div className={administracion.navegaciones}>
              <button onClick={navegarAagregarVehiculo}>Agregar Vehiculo</button>       
            </div>
            <div className={administracion.navegaciones}>
              <button onClick={navegarAlistarVehiculos}>Lista de Vehiculos</button>     
            </div>
            <div className={administracion.navegaciones}>
              <button onClick={navegarAgregarCategoria}>Agregar Categoria</button>     
            </div>
          </div>
          <img className={administracion.logo} src="/logo.ico" alt="logo" />
      </div>
      
      <div className={administracion.oculto}>
        <h2>Panel de Administracion no disponible en dispositivos móviles</h2>
      </div>
    </>

  )
}

export default Administracion