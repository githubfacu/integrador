import React from 'react'
import { useNavigate } from 'react-router-dom'
import FormularioVehiculos from '../../Componentes/Formularios/FormularioVehiculos'
import { navegacion, contenedor, oculto } from '../../Estilos/Administracion/AgregarVehiculos.module.css'

const AgregarVehiculos = () => {

  const navigate = useNavigate()

  const irAtras = () => {
    navigate(-1)
  }

  return (
    <>
      <div className={contenedor}>
        <div className={navegacion}>
          <h3>Panel de Administracion</h3>
          <span onClick={irAtras} style={{cursor: 'pointer'}}><i style={{color: '#373866'}} className="fa-solid fa-arrow-left fa-2xl"></i></span>   
        </div>
          <h2>Registro de Vehiculos</h2>
          <FormularioVehiculos />
      </div>

      <div className={oculto}>
        <h2>Panel de Administracion no disponible en dispositivos móviles</h2>
      </div>
    </>
  )
}

export default AgregarVehiculos