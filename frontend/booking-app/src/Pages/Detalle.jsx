import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { cartaDelVehiculo, definicion, descripcion, imagenes, imgPrincipal, caracteristicas } from '../Estilos/Detalle.module.css'
import GaleriaDeImagenes from '../Componentes/GaleriaDeImagenes'

const Detalle = () => {

  const [vehiculo, setVehiculo] = useState({})
  const [verMas, setVerMas] = useState(false)
  const navigate = useNavigate()
  const {id} = useParams()
  const url = 'http://3.83.149.57:8080/producto'
  const endpoint = `${url}/${id}`

  useEffect(() =>{
    fetch(endpoint)
    .then(res=>{
      if(res.ok){
        return res.json()
      }
      else{
        throw new Error(`Error en la solicitud: ${res.status}`)
      }
    })
    .then((data)=>setVehiculo(data))
    .catch(error=>console.error(error))
  }, [])

  const verGaleriaDeImagenes = () => {
    setVerMas(true)
  }

  const salirDeGaleriaDeImagenes = () => {
    setVerMas(false)
  }

  const irAtras = () => {
    navigate(-1)
  }

  console.log(verMas);

  return (
    <div className={cartaDelVehiculo}>
      <div className={definicion}>
        <div>
          <h2>Ford Focus</h2>
          <h3>SUV</h3>          
        </div>
        <span onClick={irAtras}><i className="fa-solid fa-arrow-left fa-2xl"></i></span>
      </div>
      <section>
        <div className={imagenes}>
          <div className={imgPrincipal}>
            <img src="/public/Images/suv_1.jpg" alt="" />
          </div>
          <article>
            <img src="/Images/auto-backup/inside-car-5-3228254434.jpg" alt="Imagen del vehiculo (modelo-categoria)" />
            <img src="/Images/auto-backup/rueda_llanta.jpg" alt="Imagen del vehiculo (modelo-categoria)" />
            <img src="/Images/auto-backup/puerta.jpg" alt="Imagen del vehiculo (modelo-categoria)" />
            <img src="/Images/auto-backup/88e7ce2f46a39e9b9c2cfa6f7d7e4212-2964408913.jpg" alt="Imagen del vehiculo (modelo-categoria)" />

          </article>
          <h5 onClick={verGaleriaDeImagenes}>Ver más...</h5>
        </div>
        <div className={descripcion}>

            <p>Vehiculo Ford Focus, Tiene una caja manual de 5 cambios, con 200 caballos de fuerza, este vehiculo funciona a base de Gasolina(Nafta), es un automovil de 4 puertas con mucha amplitud en la parte trasera y delante del vehiculo y con un espacioso baul.</p>
            
            <div className={caracteristicas}  >
                      <h2>Características</h2>

              <ul >
               <div>
                  <li><span><i className="fa-solid fa-suitcase"></i></span><p>2</p></li>
                  <li><span><i className="fa-solid fa-people-group"></i></span><p>5</p></li>
                  <li><span><i className="fa-solid fa-car-side"></i></span><p>Automatico</p></li>
                </div>
               <section>
                 <li><span><i className="fa-solid fa-gas-pump"></i></span><p>Gasolina</p></li>
                  <li><span><i className="fa-brands fa-bluetooth"></i></span><p>Bluetooth</p></li>
                  <li><span><i className="fa-solid fa-snowflake" ></i></span><p>A/C</p></li>
               </section>
              </ul>
            </div>


        </div>
      </section>

      {verMas && <GaleriaDeImagenes salirDeGaleriaDeImagenes={salirDeGaleriaDeImagenes}/>}
    </div>
  )
}

export default Detalle