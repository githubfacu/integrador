import { Route, Routes } from 'react-router-dom'
import Header from './Componentes/Header'
import Home from './Pages/Home'
import Categorias from './Pages/Categorias'
import Detalle from './Pages/Detalle'
import User from './Pages/User'
import Administracion from './Pages/Administracion/Administracion'
import AgregarVehiculos from './Pages/Administracion/AgregarVehiculos'
import ListaDeVehiculos from './Pages/Administracion/ListaDeVehiculos'


import Footer from './Componentes/Footer'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import './App.css'
import AgregarCategoria from './Pages/Administracion/AgregarCategoria'
 





function App() {

  return (
    <>
      <ToastContainer />
      <Header />

      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/categorias" element={<Categorias/>} />
        <Route path="/detalle/:id" element={<Detalle/>} />
        <Route path="/administracion" element={<Administracion/>} />
        <Route path="/administracion/agregarvehiculo" element={<AgregarVehiculos/>} />
        <Route path="/administracion/gestiondevehiculos" element={<ListaDeVehiculos/>} />
        <Route path="/administracion/agregarcategoria" element={<AgregarCategoria/>} />
        <Route path="/user" element={<User/>} />
      </Routes>

      <Footer />
    </>

  )
}

export default App
