import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import estiloBuscador from "../Estilos/Buscador.module.css";
import Cardbuscador from './Cardbuscador';

const Buscador = () => {

  const [categoria, setCategoria] = useState('');
  const [fechaInicio, setFechaInicio] = useState(null);
  const [fechaFin, setFechaFin] = useState(null);

  const [datos, setDatos] = useState([])

  const url = 'http://localhost:8080'
  const endpoint = 'http://3.83.149.57:8080'

  const payload = {
    categoria: categoria,
    fechaInicio: fechaInicio,
    fechaFin: fechaFin
  }

  const configuraciones = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
    // body: JSON.stringify(payload)
  }

  console.log(datos)

  const handleSearch = () => {

    fetch(`${url}/products/public`, configuraciones)
    .then(res=>{return res.json()})
    .then(data=>{
      console.log(data.products);
      // const dataFiltroCategoria = data.filter(item => item.categoria === payload.categoria)


      setDatos(data.products)
    })

  }



  return (
    <div className={estiloBuscador.divPrincipal}>
      <div className={estiloBuscador.divContenido}>

        <div className={estiloBuscador.categoria}>
          <label htmlFor="categoria">Categoría:</label>
          <select
            id="categoria"
            value={categoria}
            onChange={(e) => setCategoria(e.target.value)}
          >
            <option value="">Seleccione una categoría</option>
            <option value="Sedan">Sedan</option>
            <option value="SUV">SUV</option>
            <option value="Compacto">Compacto</option>
            <option value="Deportivo">Convertible</option>
            <option value="Pickup">4x4</option>
            <option value="Electrico">Eléctricos</option>
          </select>
        </div>
        <div className={estiloBuscador.fechaInicio}>
          <label htmlFor="fechaInicio">Fecha de inicio:</label>
          <DatePicker
            selected={fechaInicio}
            onChange={(date) => setFechaInicio(date)}
            dateFormat="yyyy-MM-dd"
            placeholderText="Fecha Inicio"
          />
        </div>
        <div className={estiloBuscador.fechaFin}>
          <label htmlFor="fechaFin">Fecha de fin:</label>
          <DatePicker
            selected={fechaFin}
            onChange={(date) => setFechaFin(date)}
            dateFormat="yyyy-MM-dd"
            placeholderText="Fecha Fin"
          />
        </div>
        <button className={estiloBuscador.botonBuscar} onClick={handleSearch}>Buscar</button>
      </div>


      <div className={estiloBuscador.resultadoBuscador}>
          {datos && 
              datos.map((vehiculo) => {
                return <div className={estiloBuscador.carddt}><Cardbuscador vehiculo={vehiculo}/></div>
              })
          }          
      </div>


    </div>
  );
};

export default Buscador;
