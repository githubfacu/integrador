import React from 'react'

const GaleriaDeImagenes = ({salirDeGaleriaDeImagenes}) => {

  return (
    <div style={{
        position: 'fixed',
        top: '0',
        left: '0',
        width: '100%',
        height: '100%',
        zIndex: '101'
        }}>
        <div style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
            background: 'rgba(0, 0, 0, .4)'
            }}>
        </div>
        <section style={{
            position: 'absolute',
            top: '12%',
            left: '20%',
            width: '60%',
            height: '72%',
            background: 'rgba(0, 0, 0, .9)',
            borderRadius: '8px'
            }}>
              <span style={{
                position: 'absolute', 
                display: 'flex',
                color: 'white', 
                top: '0', 
                right: '-38px',
                width: '35px',
                height: '35px',
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: '8px',
                background: 'rgba(0, 0, 0, .1)',
                cursor: 'pointer'
                }}>
                <i onClick={salirDeGaleriaDeImagenes} className="fa-solid fa-x fa-lg"></i>
              </span>
        </section>
    </div>

  )
}

export default GaleriaDeImagenes