import React from 'react'
import { itemId , nombreItem , eliminar} from '../Estilos/Administracion/ElementoDeLista.module.css'


const ElementoDeLista = ({identificacion, nombre, url, token, entidad}) => {


  const configuraciones = {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  }

  const deleteProductById = () => {
    fetch(`${url}/${identificacion}`, configuraciones)
    .then(response => {
      return response;
    })
    .then(result => {
      console.log(result);
    })
  }

  return (
    <>
        <h4 className={itemId}>{identificacion}</h4>
        <h4 className={nombreItem}>{nombre}</h4>
        <div className={eliminar}>
          <h4 onClick={deleteProductById}>Eliminar {entidad}</h4>
        </div>
    </>
  )
}

export default ElementoDeLista