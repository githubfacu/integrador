import React from 'react';
import estiloCardbuscador from '../Estilos/Cardbuscador.module.css';

const Cardbuscador = ({vehiculo}) => {

    return (
        <div className={estiloCardbuscador.cardContainer}>
            <img className={estiloCardbuscador.imagen} src={vehiculo.images[0]} alt="Auto" />

            <div className={estiloCardbuscador.info}>
                <h4 className={estiloCardbuscador.nombre}>{vehiculo.brand} {vehiculo.model}</h4>
                <h4 className={estiloCardbuscador.disponible}>Disponible</h4>                
            </div>
        </div>
    )
}

export default Cardbuscador;
