import React from 'react'
import { useNavigate } from 'react-router-dom'
import { button } from '../Estilos/BotonIrAtras.module.css'

const BotonIrAtras = () => {

    const navigate = useNavigate()

    const irAtras = () => {
        navigate(-1)
    }

  return (
    <>
        <button className={button} onClick={irAtras}>«Volver</button>
    </>
  )
}

export default BotonIrAtras