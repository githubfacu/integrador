import React, {useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom'
import estiloCard from "../Estilos/Card.module.css";

const Card = ({ categoria }) => {


  const navigate = useNavigate()
  const [state, setState] = useState(false)
  const [alto, setAlto] = useState('23%')
  const [display, setDisplay] = useState('none')

  const onClickVerMas = () => {
    sessionStorage.setItem('categoria', JSON.stringify(categoria))
    navigate('/categorias')
  }


  function MontarDesmontar(){     
      setState(!state)
      applyTransition()
  }

  useEffect(() => {
    if(state){
      setAlto('100%')
      setTimeout(()=>{
        setDisplay('block')
      },400)        
    } else{
      setAlto('23%')
      setDisplay('none')
    }

  }, [state])

  console.log(state);


  return (
    <div onClick={MontarDesmontar} className={estiloCard.card}>

      <img className={estiloCard.imagenCard} src={categoria.imageEncode} alt="Vehículo" />

      <div style={{height: `${alto}`}} className={estiloCard.tituloCategoriaCard}>
        <div className={estiloCard.head}>
          <h3>{categoria.name}</h3>

          {
            (alto === '100%') ? <span><i className="fa-solid fa-chevron-up fa-rotate-180"></i></span> :
            <span><i className="fa-solid fa-chevron-up"></i></span>            
          }
          
          <h5 onClick={onClickVerMas}>Ver Autos</h5>          
        </div>


        {
          state && 
            <p style={{display: `${display}`}}>{categoria.description}</p>
        }
      </div>

    </div>
  );
};

export default Card;