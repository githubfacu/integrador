import React, { useState, useEffect } from 'react'
import Card from "./Card";
import Carddetalle from "./Carddetalle";
import estiloBody from "../Estilos/Body.module.css"
import backgroundBody from "/Images/lamborgini_home.jpg"
import imgrecomendados from "/Images/sedan_home.png"
import Buscador from './Buscador';


const body = () => {

    const [vehiculosRecomendados, setVehiculosRecomendados] = useState([])
    const [categoriasDeAutos, setCategoriasDeAutos] = useState([])
    const token = JSON.parse(localStorage.getItem('token'))

    const url = 'http://localhost:8080'

    const configuracionesGet = {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json',
        }
      }

    useEffect(() => {
        fetch(`${url}/category/public`, configuracionesGet)
        .then(res=>res.json())
        .then((data) =>{
            console.log(data);
            setCategoriasDeAutos(data) 
        })
    }, [])

    useEffect(() => {
        fetch(`${url}/products/public`)
        .then(res=>{
            if(!res.ok){
                throw new Error(res.status)
            }
            else{
                return res.json()
            }
        })
        .then((data)=>setVehiculosRecomendados(data.products))
        .catch((error)=>console.error(error))
    }, [])



    return (
        <div className={estiloBody.divPrincipal}>
            <div className={estiloBody.divImgBg}>
                <img style={{display:'none'}} className={estiloBody.imgBackground} src={backgroundBody} alt="imagen lamborgini" />
                <Buscador />
            </div>


            <div className={estiloBody.divCategorias}>
                <div className={estiloBody.divTituloCategorias}>
                    <h2>Categorias de Vehiculos</h2>
                </div>

                <div className={estiloBody.divContenidoCategorias}>

                    {
                        categoriasDeAutos && 
                        categoriasDeAutos.map((categoria)=>{
                            return <>
                                <Card categoria={categoria} />
                            </>
                        })
                    }


                </div>
            </div>

            <div className={estiloBody.divRecomendados}>
                <div className={estiloBody.divTituloRecomendados}>
                    <h2>Vehiculos Recomendados</h2>
                </div>

                <div className={estiloBody.Recomendados}>
                    
                    {
                        vehiculosRecomendados &&
                        vehiculosRecomendados.map((item) => (
                            <div key={item.id} className={estiloBody.divContenidoRecomendados_1}>
                                <Carddetalle vehiculo={item} />
                            </div>
                        ))
                    }

                </div>

                <div className={estiloBody.divContenidoRecomendados_2}>
                    <img className={estiloBody.imgrecomendados} src={imgrecomendados} alt="imagen sedan" />
                </div>                
            </div>

        </div>
    )
}

export default body;
