import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { estiloHeader , imagenLogo, links, responsive, burguer, elMenu, avatar, logoHeader, usersIcons, userOptions} from '../Estilos/Header.module.css'
import FormularioRegistro from './Formularios/FormularioRegistro'
import FormularioIngreso from './Formularios/FormularioIngreso'

const Header = () => {

  const navigate = useNavigate()
  const [mostrar, setMostrar] = useState(false)
  const [verFormRegistro, setVerFormRegistro] = useState(false)
  const [verFormIngreso, setVerFormIngreso] = useState(false)
  const [superSwitch, setSuperSwitch] = useState(false)

  const [menuSwitch, setMenuSwitch] = useState(false)
  const [menuHeigth, setMenuHeigth] = useState('0px')
  const [display, setDisplay] = useState('none')

  const [userOptionsSwitch, setUserOptionsSwitch] = useState(false)
  const [optionsHeigth, setOptionsHeigth] = useState('0')
  const [optionsDisplay, setOptionsDisplay] = useState('none')

  const tokenUsuario = JSON.parse(localStorage.getItem('token'));
  const rolUsuario = JSON.parse(localStorage.getItem('rolUsuario'));

  const nombreUsuario = JSON.parse(localStorage.getItem('nombreUsuario'));
  const apellidoUsuario = JSON.parse(localStorage.getItem('apellidoUsuario'));

  const iniciales = (str) => {
    if (str) {
      return str.charAt(0).toUpperCase();
  }}

  const inicialNombre = iniciales(nombreUsuario);
  const inicialApellido = iniciales(apellidoUsuario);
  
  const setearMenu = ()=>{
    setMostrar(!mostrar)
  }
  const navegarAdmin = () => {
    navigate('/administracion')
  }
  const navegarHome = () => {
    navigate('/')
  }
  const navegarUser = () => {
    navigate('/user')
  }
  
  const switchDelFormRegistro = () => {
    setVerFormRegistro(!verFormRegistro)
  }

  const switchDelFormIngreso = () => {
    setVerFormIngreso(!verFormIngreso)
  }

  const superSwitchClick = () => {
    setVerFormRegistro(!verFormRegistro)
    setVerFormIngreso(!verFormIngreso)
  }


  useEffect(() => {
    if(mostrar){
      setMenuHeigth('315px')
      setTimeout(() => {
        setDisplay('block')
      }, 300)
    }
    else{
      setMenuHeigth('0px')
      setDisplay('none')
    }
  }, [mostrar])


  const optionUserMenu = () =>{
    setUserOptionsSwitch(!userOptionsSwitch)
  }

  useEffect(() => {
    if(userOptionsSwitch){
      setOptionsHeigth('1')
      setOptionsDisplay('block')
    }
    else{
      setOptionsHeigth('0')
      setTimeout(() => {
        setOptionsDisplay('none')
      }, 100)
    }
  }, [userOptionsSwitch])

  const cerrarSesion = () => {
    setUserOptionsSwitch(false)

    if(confirm("¿Estás seguro que quieres Cerrar Sesion?")){
      alert('Sesion Cerrada')
      localStorage.clear()
      navigate('/')
    }
  }

  return (
    <div className={estiloHeader}>

      <div className={imagenLogo} >
        <div className={responsive}>
          
          <div className={burguer}>
            <span onClick={setearMenu}>
              <i  className="fa-solid fa-bars fa-xl" ></i>
            </span>            
          </div>


            <div className={elMenu}>
              <ul style={{height: `${menuHeigth}`}}>
                <div style={{display: `${display}`}}>
                <li><h3>Home</h3></li>
                <li><h3>Nosotros</h3></li>
                <li><h3>Categorias</h3></li>
                <li><h3>Recomendados</h3></li>
                <li><h3>Contacto</h3></li>
                <li onClick={switchDelFormRegistro}><h3>Crear Cuenta</h3></li>
                <li onClick={switchDelFormIngreso}><h3>Ingresar</h3></li>
                </div>
              </ul>                
            </div>
          

        </div>

        <img onClick={navegarHome} src="/Images/logo.png" alt="imagen logo" className={logoHeader} />
        <h2 onClick={navegarHome}>Potenciando tu viaje, simplificando tu vida</h2>

        <section>

          {
            tokenUsuario ? 
            <div className={avatar}>
                <div style={{display: `${optionsDisplay}`, opacity: `${optionsHeigth}`}} className={userOptions}>
                  <h4 onClick={navegarUser}>Perfil</h4>
                  <h4 onClick={cerrarSesion}>Cerrar sesión</h4>
                </div>
                <h3 onClick={optionUserMenu}>{inicialNombre}{inicialApellido}</h3>
            </div> 
              : 
            <div className={usersIcons}>
              <div>
                  <img src="/Images/icono_ingresar.png" alt="" className={estiloHeader.imagenBoton} />
                  <button onClick={switchDelFormIngreso}>Ingresar</button>
              </div>     
              <div>
                <img src="/Images/icono_registro.png" alt="" className={estiloHeader.imagenBoton}/>
                <button onClick={switchDelFormRegistro}>Crear cuenta</button>
              </div>               
            </div>
          }


        </section>



      </div> 

      <div className={links}>
        <a href="">Home</a>
        <a href="">Nosotros</a>
        <a href="">Categorias</a>
        <a href="">Recomendados</a>
        <a href="">Contacto</a>
        {(rolUsuario === 'ADMINISTRADOR') && <a style={{position: 'absolute', right: '24px'}} href="/administracion">Panel de Administracion</a>}
      </div>
     
      {
        verFormRegistro && <FormularioRegistro superSwitchClick={superSwitchClick} switchDelFormRegistro={switchDelFormRegistro}/>
      }
      {
        verFormIngreso && <FormularioIngreso superSwitchClick={superSwitchClick} switchDelFormIngreso={switchDelFormIngreso} />
      }

      
    </div>
  )
}

export default Header