import React from 'react';
import { useNavigate } from 'react-router-dom';
import estiloCarddetalle from "../Estilos/Carddetalle.module.css";

const Carddetalle = ({ vehiculo }) => {

    console.log(vehiculo);
    const navigate = useNavigate()

    const navegarADetalle=() => {
        navigate("/detalle/:id");

    };

    return (
        <div className={estiloCarddetalle.card} onClick={navegarADetalle}>
            <div className={estiloCarddetalle.divImagen}>
                <img className={estiloCarddetalle.imagen} src='' alt="Vehículo" />
            </div>
            <div className={estiloCarddetalle.detalles}>
                <div className={estiloCarddetalle.categoria}>{vehiculo.category.name}</div>
                <div className={estiloCarddetalle.marca}> {vehiculo.brand} </div>
                <div className={estiloCarddetalle.modelo}> {vehiculo.model}</div>
                <div className={estiloCarddetalle.detalle}>
                    <i className="fas fa-user"><span>{vehiculo.numPassengers}</span></i> 
                </div>
                {/* <div className={estiloCarddetalle.detalle}>
                    {esElectrico ? <i className="fas fa-bolt"></i> : <i className="fas fa-gas-pump"><span>{esElectrico ? 'Eléctrico' : 'Gasolina'}</span></i>} 
                </div> */}
            </div>
        </div>
    );
};

export default Carddetalle;

