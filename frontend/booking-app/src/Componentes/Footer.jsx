
import { estiloFooter, logo, contactenos, seguinos} from '../Estilos/Footer.module.css'

const Footer = () => {
  return (
  <div className={estiloFooter}>
        
        <div className={logo} >
            <img src="/Images/logo.png" alt=""  style={{width:'150px'}} />
            <p>@ 2024 PROYECTO INTEGRADOR - GRUPO 1 </p>
        </div>
        

     
        <div className={contactenos} >
            <p>CONTÁCTANOS</p>
            <div>
                <img src="/Images/icono_mail.png" alt="" style={{width:'40px', margin:'5px'}} /> 
                <p>carrental.g1@gmail.com</p>
            </div>
           
            <section>
                <img src="/Images/icono_phone.png" alt="" style={{width:'40px', margin:'5px'}}/>
                <p>123456789</p>
            </section>
            
        </div>
   
   
   
        <div className={seguinos} >
            <p>SÍGUENOS</p>
            <a href="https://www.instagram.com/carrental.g1/"><img src="/Images/icono_instagram.png" alt=""  style={{margin:'10px'}}/> </a>
            <img src="/Images/icono_youtube.png" alt=""  style={{margin:'10px'}}/>
            <a href="https://www.facebook.com/people/Carrental-Cars/pfbid0ASZq2VW2Pq7QXeECFoosh9RENotT1PifekTujajKDrgPb6UFiqQCQgAc1ZcM796Rl/"><img src="/Images/icono_facebook.png" alt="" style={{margin:'10px'}} /></a>
            
            <a href="https://twitter.com/carrentalG1"><img src="/Images/icono_x.png" alt="" style={{margin:'10px'}}/></a>
        
        
        
        </div>











    </div>
  )
}

export default Footer