import React, { useState } from 'react'
import useInput from '../../Hooks/useInput'
import Container from '../../Estilos/Formularios/FormularioRegistro.module.css'

const FormularioRegistro = ({switchDelFormRegistro, superSwitchClick}) => {

  const [errores, setErrores] = useState({})
  const [errorNotificacion, setErrorNotificacion] = useState(false)

  const url = ''
  const nombre = useInput('text')
  const apellido = useInput('text')
  const email = useInput('text')
  const contrasenia = useInput('text')

  const validarNombre = (nombre) => {
    const sinEspacios = nombre.trim()
    if(sinEspacios.length > 1){
      return true
    } else{
      return false
    }
  }

  const validarApellido = (apellido) => {
    const sinEspacios = apellido.trim()
    if(sinEspacios.length > 1){
      return true
    } else{
      return false
    }
  }

  const validarEmail = (email) => {
    const regexp = /^[^\.\s][\w\-]+(\.[\w\-]+)*@([\w-]+\.)+[\w-]{2,}$/gm

    if(regexp.test(email)){
      return true
    } else{
      return false
    }
  }

  const validarContrasenia = (contrasenia) => {
    const sinEspacios = contrasenia.trim()

    const contraseniaArr = sinEspacios.split("")

    const validarNumero = contraseniaArr.some((caracter) => {
      if(isNaN(caracter)){
        return false
      } else{
        return true
      }
    })

    if(sinEspacios.length > 5 && validarNumero){
      return true
    } else{
      return false
    }
  }

  const validaciones = () => {

    const error = {}

    const nombreValido = validarNombre(nombre.value)
    const apellidoValido = validarApellido(apellido.value)
    const contraseniaValido = validarContrasenia(contrasenia.value)
    const emailValido = validarEmail(email.value)

    if(!nombreValido){
      error.nombre = true
    }else{
      error.nombre = false
    }
    if(!apellidoValido){
      error.apellido = true
    }else{
      error.apellido = false
    }
    if(!emailValido){
      error.email = true
    }else{
      error.email = false
    }
    if(!contraseniaValido){
      error.contrasenia = true
    }else{
      error.contrasenia = false
    }

    return error
  }

  const payload = {
    user: {
      name: nombre.value,
      surname: apellido.value,
      email: email.value,
      password: contrasenia.value
    }
  }

  const configuraciones = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  }

  console.log(payload);

  const handleSubmitForm = (e) => {
    e.preventDefault()

    const erroresValidacion = validaciones()
    setErrores(erroresValidacion)

    console.log(erroresValidacion)

    if(erroresValidacion.nombre === false && erroresValidacion.apellido ===false && erroresValidacion.email ===false && erroresValidacion.contrasenia ===false){
      fetch(url, configuraciones)
      .then(res=> {
        if(!res.ok){
          throw new Error (res.status)
        }
        else{
          res.json()
        }
      })
      .then((data) => {
        localStorage.setItem('user', json.stringify(data))
        alert("Bienvenido!")        
      })
      .catch(error=>{
        console.error('Hay un error!', error)
        setErrorNotificacion(true)
      })
      nombre.onChange({target : { value : ''}})
      apellido.onChange({target : { value : ''}})
      email.onChange({target : { value : ''}})
      contrasenia.onChange({target : { value : ''}})
    }
  }


  return (
    <div className={Container.fondo}>
      <div className={Container.formRegistro}>
        <h1>Crear Cuenta</h1>
        <form onSubmit={handleSubmitForm}>
          <div className={Container.inputDiv}>
            <label>Nombre</label>          
            <input {...nombre} />
            {errores.nombre && <p>+2 caracteres</p>}
          </div>
          <div className={Container.inputDiv}>
            <label>Apellido</label>
            <input {...apellido} />          
            {errores.apellido && <p>+2 caracteres</p>}
          </div>
          <div className={Container.inputDiv}>
            <label>Email</label>
            <input {...email} />          
            {errores.email && <p>Email inválido</p>}
          </div>
          <div className={Container.inputDiv}>
            <label>Contraseña</label>
            <input {...contrasenia} />
            {errores.contrasenia && <p>+6 caracteres, al menos 1 número</p>}
          </div>

          <button type='Submit'>Crear Cuenta</button>
        </form>

        <div className={Container.notificacionError}>
          {
            errorNotificacion &&
            <p>Error en la solicitud</p>
          }
        </div>

        <h4>Ya tengo una cuenta!</h4><h3 onClick={superSwitchClick}>Ingresar</h3>

        <span><i onClick={switchDelFormRegistro} className="fa-solid fa-x fa-xl"></i></span>
      </div>      
    </div>


  )
}

export default FormularioRegistro