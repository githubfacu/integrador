import React, { useState } from 'react'
import useInput from '../../Hooks/useInput'
import Container from '../../Estilos/Formularios/FormularioIngreso.module.css'

const FormularioIngreso = ({switchDelFormIngreso, superSwitchClick}) => {

  const [errores, setErrores] = useState({})
  const [errorNotificacion, setErrorNotificacion] = useState(false)

  const url = 'http://localhost:8080'
  const email = useInput('text')
  const contrasenia = useInput('text')

  const validarEmail = (email) => {
    const regexp = /^[^\.\s][\w\-]+(\.[\w\-]+)*@([\w-]+\.)+[\w-]{2,}$/gm

    if(regexp.test(email)){
      return true
    } else{
      return false
    }
  }

  const validarContrasenia = (contrasenia) => {
    const sinEspacios = contrasenia.trim()

    const contraseniaArr = sinEspacios.split("")

    const validarNumero = contraseniaArr.some((caracter) => {
      if(isNaN(caracter)){
        return false
      } else{
        return true
      }
    })

    if(sinEspacios.length > 7 && validarNumero){
      return true
    } else{
      return false
    }
  }

  const validaciones = () => {

    const error = {}

    const contraseniaValido = validarContrasenia(contrasenia.value)
    const emailValido = validarEmail(email.value)

    if(!emailValido){
      error.email = true
    }else{
      error.email = false
    }
    if(!contraseniaValido){
      error.contrasenia = true
    }else{
      error.contrasenia = false
    }

    return error
  }

  const payload = {
      email: email.value,
      password: contrasenia.value
  }

  const configuraciones = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  }

  console.log(payload);

  const handleSubmitForm = (e) => {
    e.preventDefault()

    const erroresValidacion = validaciones()
    setErrores(erroresValidacion)

    console.log(erroresValidacion)

    if(erroresValidacion.email === false && erroresValidacion.contrasenia === false){

      fetch(`${url}/user/public/login`, configuraciones)
      .then(resp=> {
        if(!resp.ok){
          throw new Error [(resp.status), (resp.statusText)]
        }
        else{
          console.log(resp);
          console.log([(resp.status), (resp.statusText)]);
          return resp.json()
        }
      })
      .then((data) => {
        setErrorNotificacion(false);

        console.log(data);

        localStorage.setItem('token', JSON.stringify(data.token));
        localStorage.setItem('nombreUsuario', JSON.stringify(data.userName));
        localStorage.setItem('apellidoUsuario', JSON.stringify(data.lastName));
        localStorage.setItem('rolUsuario', JSON.stringify(data.rol));
        localStorage.setItem('emailUsuario', JSON.stringify(data.email));


        alert("¡Bienvenido!");
        switchDelFormIngreso()
      })
      .catch((error)=>{
        console.error(error)
        alert('Error al intentar acceder al sitio')
      })

      email.onChange({target : { value : ''}})
      contrasenia.onChange({target : { value : ''}})
    }
  }

  return (
    <div className={Container.fondo}>
      <div className={Container.form}>
        <h1>Inicio de Sesión</h1>
        <form onSubmit={handleSubmitForm}>
          <div className={Container.inputDiv}>
            <label>Email</label>
            <input {...email} />          
            {errores.email && <p>Email inválido</p>}
          </div>
          <div className={Container.inputDiv}>
            <label>Contraseña</label>
            <input {...contrasenia} />
            {errores.contrasenia && <p>+8 caracteres, al menos 1 número</p>}
          </div>

          <button type='Submit'>Iniciar Sesión</button>
        </form>

        <div className={Container.notificacionError}>
          {
            errorNotificacion &&
            <p>Los datos de inicio de sesion son incorrectos</p>
          }
        </div>

        <h4>No estas registrado?</h4><h3 onClick={superSwitchClick}>Crear una cuenta</h3>

        <span onClick={switchDelFormIngreso}><i className="fa-solid fa-x fa-xl"></i></span>
      </div>      
    </div>
  )
}

export default FormularioIngreso