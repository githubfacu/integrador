# Stage 1: Build the React application
FROM node:latest as build-stage

WORKDIR /app/booking-app

# Copy the package.json and package-lock.json
COPY booking-app/package*.json ./

# Install dependencies
RUN npm install

# Copy the React source code and vite configuration
COPY booking-app/ ./

# Build the app
RUN npm run build

# Manually copy the Images folder to the build output (after vite build)
# RUN mkdir -p dist/assets/Images
# RUN cp -R src/assets/Images dist/assets/

# Stage 2: Serve the app with Nginx
FROM nginx:alpine as production-stage

# Remove the default Nginx configuration
RUN rm /etc/nginx/conf.d/default.conf

# Copy the custom Nginx configuration file to the Nginx directory
COPY config/nginx.conf /etc/nginx/conf.d/

# Copy the built app from the previous stage
COPY --from=build-stage /app/booking-app/dist /usr/share/nginx/html

# Expose port 80 for the web server
EXPOSE 80

# Start Nginx by default when the container starts
CMD ["nginx", "-g", "daemon off;"]
